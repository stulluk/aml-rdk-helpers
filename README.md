# aml-rdk-helpers

Bunch of simple helpers for RDK, intended for debugging & testing on target


## Installation

1) cd to ${TOPDIR}/meta-amlogic/recipes-multimedia/

2) just clone this repo there

3) add "aml-rdk-helpers" to image config. (Such as rdk-generic-mediaclient-image.bb"

4) rebuild image ( no need uboot or kernel rebuild)

## Testing

Your image should include these scripts or binaries under /usr/bin/


