SUMMARY  = "aml-rdk-helpers"
DESCRIPTION = "this bb provides some helpers such as dvb-scan, sift, dtc "
LICENSE  = "CLOSED"

SRC_URI += " file://qw-rdk-test.sh"
SRC_URI += " file://dvbt-scan.sh"
SRC_URI += " file://dvbs-scan.sh"
SRC_URI += " file://dtc"
SRC_URI += " file://sift"
SRC_URI += " file://stop-rdk-services.sh"
SRC_URI += " file://get_provisioning_status.sh"
SRC_URI += " file://keypad-voltage.sh"
SRC_URI += " file://launch-hbbtv-atmos.sh"
DEPENDS += "jq"
DEPENDS += "bash"

do_install_append() {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/qw-rdk-test.sh ${D}${bindir}
	install -m 0755 ${WORKDIR}/dvbt-scan.sh ${D}${bindir}
	install -m 0755 ${WORKDIR}/dvbs-scan.sh ${D}${bindir}
	install -m 0755 ${WORKDIR}/dtc ${D}${bindir}
	install -m 0755 ${WORKDIR}/sift ${D}${bindir}
	install -m 0755 ${WORKDIR}/stop-rdk-services.sh ${D}${bindir}
	install -m 0755 ${WORKDIR}/get_provisioning_status.sh ${D}${bindir}
	install -m 0755 ${WORKDIR}/keypad-voltage.sh ${D}${bindir}
	install -m 0755 ${WORKDIR}/launch-hbbtv-atmos.sh ${D}${bindir}
}


