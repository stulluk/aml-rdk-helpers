#/bin/bash
for service in tr69agent nlmon wifi logrotate wpa_cli bluetooth cecdaemon mocastatuslogger ping-telemetry telemetry2_0 iarmbusd webconfig parodus pqserver deepsleepmgr subtitleserver ermgr pwrmgr sysmgr mfrmgr irmgr audiocapturemgr audioserver tvserver dsmgr dobby btmgr tr69hostif netsrvmgr dtvkit wpeframework network-connection-stats network-connection-stats.timer logrotate.timer xdial ip-iface-monitor restart-timesyncd ip-setup-monitor vitalprocess-info vitalprocess-info.timer
do
	printf "stopping service %s\n" "${service}"
	systemctl stop ${service}

done

