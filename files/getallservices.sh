#!/bin/bash

DEV_IP_PORT="192.168.1.227:9998"

function get_number_of_channels(){

        numofch=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getNumberOfServices", "json": [] }}' | jq -r '.result | fromjson.data')
        echo $numofch
}

function list_channels() {

        numofch=$(get_number_of_channels)
        if [ "${numofch}" -lt 1 ]
        then
                printf "No channels in DB!!!\n"
                exit 1
        fi

        printf "Number of Channel in DB is %d\n" "${numofch}"

        #Lets read whole CHLIST chunk-by-chunk, 50 channels at a time
        offset=0
        increment=50

        while [[ $offset -lt $numofch ]]
        do
		set -x
                curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getListOfServicesByIndex", "json": ["cur","all",'$offset','$increment'] }}' | jq -c '.result | fromjson.data[] ' | jq -r '"\(input_line_number+'$offset')) \(.name) ** \(.transponder) ** \(.sig_name)"' | column -t -s '**'
		set +x
                offset=$((offset+increment))
        done
}

function construct_services_array(){

        numofch=$(get_number_of_channels)
        if [ "${numofch}" -lt 1 ]
        then
                printf "No channels in DB!!!\n"
                exit 1
        fi

        # Define the base JSON request
        base_request='{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getListOfServicesByIndex", "json": ["cur","all",'

        offset=0
        increment=5

        services=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d "$base_request$offset,$increment]}}" | jq '.result | fromjson.data')
        offset=$((offset+increment))

        while [[ $offset -lt $numofch ]]
        do
                # Make the curl request and append the result to the services variable
                curservices=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d "$base_request$offset,$increment]}}" | jq '.result | fromjson.data')
                services=$(echo $services $curservices | jq -s add)
                offset=$((offset+increment))
        done

        # Print the combined JSON response
        echo "$services"


}

function watch_tv() {
        # Select a channel
        services=$(construct_services_array)
        #printf "\n$services\n"

        # Check if the result field is an empty string or contains an error message
        is_valid_json=$(echo "$services" | jq -e . >/dev/null 2>&1 ; echo ${PIPESTATUS[1]})
        if [ "${is_valid_json}" == "1" ]
        then
                printf "\n\n************\nNo channels, please SCAN first.\n*************\n\n\n"
        elif [ "${is_valid_json}" == "4" ]
        then
                printf "\n\n************\nNo VALID JSON, system ERROR\n*************\n\n\n"
        else
                while [ 1 ]; do

                        printf "\nCHANNEL LIST\n"
                        # Parse the JSON and print the names and URIs
                        list_channels
                        #curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getListOfServicesByIndex", "json": ["cur","all",0,50] }}' | jq -c '.result | fromjson.data[]' | jq -r '"\(input_line_number)) \(.name) ** \(.transponder) ** \(.sig_name)"' | column -t -s '**'
                        printf "\nEnter q or Q to back to main menu..\n\n"
                        read -p "Select a channel (enter ch number) or back to main menu (enter \"q\" ): " channel_choice
                        printf "Choice is %s\n" ${channel_choice}

                        if [ $channel_choice == "q" ] || [ $channel_choice == "Q" ]; then
                                printf "Returning back to main menu...\n"
                                return
                        fi

                        # Get the URI based on the selected channel name
                        selected_uri=$(echo "$services" | jq -r --argjson choice "$channel_choice" '.[$choice-1] | .uri')

                        printf "\nSelected URI is $selected_uri\n"

                        #stop_player
                        #prepare_display

                        # Command to start watching the selected channel
                        printf "\nStart playing ${selected_uri}...\n"
                        #curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Player.play", "json": [0,"'"$selected_uri"'",false,false,0,"",""] }}'
                        printf "\n\n"
                done
        fi

}

list_channels

#watch_tv
