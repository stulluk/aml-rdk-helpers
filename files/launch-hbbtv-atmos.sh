#!/usr/bin/env bash

curl 'http://127.0.0.1:9998/jsonrpc' -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.createDisplay", "params": { "client": "test-0", "displayName": "test-0" }}' ; echo
curl 'http://127.0.0.1:9998/jsonrpc' -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.moveToFront", "params": { "client": "test-0" }}' ; echo
curl 'http://127.0.0.1:9998/jsonrpc' -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.setFocus", "params": { "client": "test-0" }}' ; echo

export XDG_RUNTIME_DIR=/run
export WESTEROS_GL_GRAPHICS_MAX_SIZE=1920x1080
export WESTEROS_SINK_AMLOGIC_USE_DMABUF=1
export WESTEROS_GL_USE_REFRESH_LOCK=1
export WESTEROS_SINK_USE_FREERUN=1
export WESTEROS_GL_USE_AMLOGIC_AVSYNC=1
export LD_PRELOAD=/usr/lib/libwesteros_gl.so.0.0.0
export WAYLAND_DISPLAY=test-0

curl -X POST http://127.0.0.1:9998/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "Controller.1.activate", "params": { "callsign": "WebKitBrowser" }}'
curl -X POST http://127.0.0.1:9998/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "WebKitBrowser.1.localstorageenabled", "params": true}'
curl -X POST http://127.0.0.1:9998/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "WebKitBrowser.1.state", "params": "resumed"}'
curl -X POST http://127.0.0.1:9998/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.setFocus", "params": { "client": "WebKitBrowser" }}'
curl -X POST http://127.0.0.1:9998/jsonrpc -d '{"jsonrpc":"2.0","id":3, "method":"WebKitBrowser.1.url", "params": "http://ms12.streaming.dolby.com/v26/app/index_261.html"}'
curl -X POST http://127.0.0.1:9998/jsonrpc -d '{"jsonrpc":"2.0","id":3, "method":"org.rdk.RDKShell.1.setVisibility","params":{"client":"residentapp","visible": false}}'
