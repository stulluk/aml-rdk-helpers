#!/usr/bin/env bash

#set -x

DEV_IP_PORT="127.0.0.1:9998"
SATNAME="TestSAT"

#Diseqc1.0 Port number
DISEQC1_0_PORT=1

#Diseqc1.1 Port number
DISEQC1_1_PORT=1

function is_dtvkit_ready(){

        if systemctl is-active --quiet  dtvkit
        then
                printf "DTVkit service is ready, starting..\n"
        else
                printf "DTVkit service is NOT ready, please wait a while and re-run this script..\n"
                exit 1
        fi

}

function print_oneliner_usage(){

	printf "Error! Usage : %s freq polarity symrate DVBS\n" "$(basename "${0}")"
	printf "Example : %s 12685 V 27500 DVBS2 \n" "$(basename "${0}")"

}

function disable_deinterlacer() {

	echo 0 > /sys/module/decoder_common/parameters/v4lvideo_add_di
	echo 45 0x100000 > /sys/kernel/debug/di_top/mw_di

}

function enable_tuner_debug() {

	#enable debug commands
	echo 0xfff > /sys/module/aml_media/parameters/aml_demod_debug
	echo diseqc_dbg 255 > /sys/class/dtvdemod/attr

}

function disable_tuner_debug() {

	echo 0x0 > /sys/module/aml_media/parameters/aml_demod_debug
	echo diseqc_dbg 0 > /sys/class/dtvdemod/attr

}

function set_resolution_framerate(){

	# Enable debug mode otherwise can not select a new framerate
	echo "1" > /sys/class/display/debug
	MODE="/sys/class/display/mode"

	printf "\nResolutions: \n"
	echo "1) 1080p50Hz"
	echo "2) 1080p60Hz"
	echo "3) 2160p50Hz"
	echo "4) 2160p60Hz"

	printf "\n"
	read -p "Select resolutions:" RESFRM
	case ${RESFRM} in
		1)
			echo "1080p50hz" > ${MODE}
			;;
		2)
			echo "1080p60hz" > ${MODE}
			;;
		3)
			echo "2160p50hz" > ${MODE}
			;;
		4)
			echo "2160p60hz" > ${MODE}
			;;
		*)
			printf "Wrong input, doing nothing!!!...\n"
			;;
	esac
		

}

function update_tp_info() {

	read -p "Enter frequency in Mhz ( such as 12685): " FREQ
	read -p "Enter polarity ( V or H ): " POL
	read -p "Enter Symbol Rate ( Such as 27500) : " SYMRATE
	read -p "Enter DVBS/S2 type (Such as DVBS or DVBS2 ) : " DVBTYPE

}

function prepare_display() {
	#Prepare environment
	echo 1 > /sys/kernel/debug/dri/0/vpu/blank

	curl 'http://'${DEV_IP_PORT}'/jsonrpc' -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.createDisplay", "params": { "client": "test-0", "displayName": "test-0" }}'
	echo
	curl 'http://'${DEV_IP_PORT}'/jsonrpc' -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.moveToFront", "params": { "client": "test-0" }}'
	echo
	curl 'http://'${DEV_IP_PORT}'/jsonrpc' -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.setFocus", "params": { "client": "test-0" }}'
	echo
	export XDG_RUNTIME_DIR=/run
	export WESTEROS_GL_GRAPHICS_MAX_SIZE=3840x2160
	# # USE_DMABUF is needed
	export WESTEROS_SINK_AMLOGIC_USE_DMABUF=1
	export PLAYREADY_OP_DIGITAL_LEVEL=200
	export WESTEROS_GL_USE_REFRESH_LOCK=1
	export WESTEROS_SINK_USE_FREERUN=1
	export WESTEROS_GL_USE_AMLOGIC_AVSYNC=1
	export WESTEROS_SINK_USE_ESSRMGR=1
	export LD_PRELOAD=libwesteros_gl.so.0.0.0
	export WAYLAND_DISPLAY=test-0
}

function back_to_launcher() {
	# After test, recover OSD
	echo 0 > /sys/kernel/debug/dri/0/vpu/blank
	# recover OSD focus
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.setFocus", "params": { "client": "residentapp" }}'
}

function stop_scan() {
	printf "\nStop Scanning...\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.finishSearch", "json": [ true ]}}'
}

function stop_tune() {
	printf "\nStop Tuning..\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.tuneActionStop", "json": [ ] }}'
}

function stop_player() {

	#First stop scanning
	stop_scan
	#stop tuning
	stop_tune
	#Now stop player
	printf "\nStop Player...\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Player.stop", "json": [0] }}'
	printf "\n"
}

function get_satellite_list() {

	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getSatellites", "json": [""] }}' | jq -r '.result | fromjson | .data[].name'

}

function delete_single_sat() {

	satname="$1"

	newsatname=$(echo "${satname}" | sed 's/\*\*/  /g ; s/\*/ /g')

	printf "SINGLE DELETE: We will delete %s\n" "${newsatname}"

	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d "{\"jsonrpc\": \"2.0\",\"id\": 1,\"method\": \"DTV.1.Invoke\", \"params\": { \"command\": \"Dvbs.deleteSatellite\", \"json\": [\"${newsatname}\"] }}" | jq

}



function delete_all_satellites() {

	get_satellite_list > tempsatlist.txt

	while read -r satellite; do
		printf "${satellite}\n"
		if [ "${satellite}" != "Turksat2/3/4A" ]; then
			printf "\n%s will be deleted\n" "${satellite}"

			#rename the satellite name so that double spaces will be handled
			newsatname=$(printf "%s" "${satellite}" | sed 's/  /\*\*/g ; s/ /\*/g')
			printf "we will delete %s\n" "${newsatname}"
			delete_single_sat "${newsatname}"
		fi
	done < tempsatlist.txt

	#delete telstar 12 seperately
	printf "\nDeleting telstar..\n"
	curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.deleteSatellite", "json": ["Telstar 12 "] }}'

}

function delete_all_lnbs() {

	for lnb_num in $(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getLnbs", "json": [ ] }}' | jq -r '.result | fromjson.data[] | .lnb'); do
		printf "\nDeleting lnb ${lnb_num} ...\n"
		curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.deleteLnb", "json": [ "'${lnb_num}'" ] }}'
	done

}

function select_lnb() {

	#set -x
	local json_data="$1"
	local selected_lnb=$(echo "$json_data" | jq -r '.result | fromjson.data[] | select(.low_local_oscillator_frequency == 9750 and .high_local_oscillator_frequency == 10600 and .c_switch == '$DISEQC1_0_PORT' and .u_switch == '$DISEQC1_1_PORT' ) | .lnb' | head -n1)
	#local selected_lnb=$(echo "$json_data" | jq -r '.result | fromjson.data[] | select(.c_switch == '$DISEQC1_0_PORT' and .u_switch == '$DISEQC1_1_PORT' ) | .lnb' | head -n1)
	echo "$selected_lnb"
	#set +x
}

function unselect_tp() {

	#First check if there was a TP defined before or not

	if [ -z ${FREQ} ]
	then
		printf "You never searched for a TP before, not unselecting any TP...\n"
		return
	fi

	# Un-Select current TP
	printf "\nUn-Select our TP: %s %s %s ...\n" "${FREQ}" "${POL}" "${SYMRATE}"
	curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.selectTransponder", "json": [ "'$SATNAME'", "'${FREQ}${POL}${SYMRATE}'", false ] }}'
	printf "\n\n"
}

function construct_services_array(){
	
	numofch=$(get_number_of_channels)
	if [ "${numofch}" -lt 1 ]
	then
		printf "No channels in DB!!!\n"
		exit 1
	fi

	# Define the base JSON request
	base_request='{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getListOfServicesByIndex", "json": ["cur","all",'

	offset=0
        increment=25
    		
	services=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d "$base_request$offset,$increment]}}" | jq '.result | fromjson.data')
	offset=$((offset+increment))

        while [[ $offset -lt $numofch ]]
	do
    		# Make the curl request and append the result to the services variable
    		curservices=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d "$base_request$offset,$increment]}}" | jq '.result | fromjson.data')
		services=$(echo $services $curservices | jq -s add)
		offset=$((offset+increment))
	done

	printf "\n"
	# Print the combined JSON response
	echo "$services"


}

function get_number_of_channels(){

	numofch=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getNumberOfServices", "json": [] }}' | jq -r '.result | fromjson.data')
	echo $numofch
}

function list_channels() {

	numofch=$(get_number_of_channels)
	if [ "${numofch}" -lt 1 ]
	then
		printf "No channels in DB!!!\n"
		exit 1
	fi

	printf "Number of Channel in DB is %d\n" "${numofch}"

	#Lets read whole CHLIST chunk-by-chunk, 50 channels at a time
        offset=0
        increment=25
        
	while [[ $offset -lt $numofch ]]
        do
		curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getListOfServicesByIndex", "json": ["cur","all",'$offset','$increment'] }}' | jq -c '.result | fromjson.data[]' | jq -r '"\(input_line_number+'$offset')) \(.name) ** \(.transponder) \(.freq/1000000) ** \(.sig_name)"' | column -t -s '**'
		offset=$((offset+increment))
        done
}

function prepare_database() {

	stop_player

	#First check if our Universal LNB exist in Database or not
	json_response=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getLnbs", "json": [ ] }}')

	selected_lnb=$(select_lnb "$json_response")

	if [[ -n "$selected_lnb" ]]; then
		printf "\nSelected LNB: $selected_lnb"
	else
		printf "\nNo LNB found with high_local_oscillator_frequency=10600"
		
		#lets add our own Universal LNB with Diseqc1.0 port = 1 and Diseqc1.1 port = 1
		printf "\nLets add our own Universal LNB with Diseqc1.0 port = %d and Diseqc1.1 port = %d..\n" "${DISEQC1_0_PORT}" "${DISEQC1_1_PORT}"

		curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.addLnb", "json": [ false, 0, 0, false, "none", '$DISEQC1_0_PORT', '$DISEQC1_1_PORT', 0, 1, 10700, 11700, 9750, "on", "auto", 11700, 12750, 10600, "on", "auto" ] }}'
		
		#wait a while to let dtvkitserver to be stable
		sleep 2
		json_response=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getLnbs", "json": [ ] }}')

		selected_lnb=$(select_lnb "$json_response")
		if [ -n "$selected_lnb" ]; then
			printf "\nSelected LNB: $selected_lnb\n"
		else
			printf "Adding our Universal LNB failed! System ERROR!!\n"
			exit 1
		fi
	fi

	# if our TestSAT doesn't exist, add it
	num_testsat=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getSatellites", "json": [""] }}' | jq -r '.result | fromjson | .data[].name' | grep -c "${SATNAME}")

	if [ ${num_testsat} -ne 1 ]
	then
		printf "\n%s does NOT exit in DB, adding it...\n" "${SATNAME}"
		# Add our SATELLITE for selected LNB
		printf "\nAdding $SATNAME...\n"
		curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.addSatellite", "json": [ "'$SATNAME'", false, 42, 15, "'$selected_lnb'" ] }}'
	fi

}

function add_tp() {
	# Add our TP
	printf "\nAdding our TP...\n"
	curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.addTransponder", "json": [ "'$SATNAME'", '$FREQ', "'$POL'", '$SYMRATE', "'$DVBTYPE'", "auto", "auto", "auto" ] }}'
}

function tp_scan() {

	#First check if there was a TP defined before or not

	if [ -z ${FREQ} ]
	then
		printf "You never searched for a TP before, please use 2nd option below to scan a TP...\n"
		return
	fi

	stop_player

	# Select our TP
	printf "\nSelect our TP: %s %s %s ...\n" "${FREQ}" "${POL}" "${SYMRATE}"
	curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.selectTransponder", "json": [ "'$SATNAME'", "'${FREQ}${POL}${SYMRATE}'", true ] }}'

	# Scan

	printf "\nScan our TP...\n"
	curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.startSearchEx", "json": ["transponder", false, "{\"lnblist\":[{\"lnb\":\"'${selected_lnb}'\"}]}"] }}'


	get_scan_status
	sleep 2
	get_signal_level
	get_scan_status

	# Wait for 1 seconds
	printf "\nSleep 1 seconds...\n"
	sleep 1

	# Stop Scan
	printf "\nStop Scanning...\n"
	stop_scan

	# List Channels
	#printf "\nList channels...\n"
	#list_channels

	printf "\nNumber of Channels in DB: %d\n" "$(get_number_of_channels)"

}


satscan() {

	#first get tplist from system
	tplist=$(curl -s -X POST http://"${DEV_IP_PORT}"/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getTransponders", "json": [ "'$SATNAME'" ] }}')

	# Statically declare Turksat TP values
	TP3="10958,V,6250,DVBS2"
	TP11="11012,H,30000,DVBS2"
	TP17="11053,H,8000,DVBS"
	TP18="11054,V,30000,DVBS2"
	TP22="11096,H,30000,DVBS"
	TP33="11180,H,29750,DVBS"
	TP35="11265,V,30000,DVBS2"
	TP36="11305,V,30000,DVBS2"
	TP48="11558,V,30000,DVBS"
	TP50="11676,V,24444,DVBS"
	TP51="11767,V,15000,DVBS2"
	TP52="11789,H,20000,DVBS2"
	TP53="11795,V,30000,DVBS2"
	TP54="11822,H,14166,DVBS2"
	TP55="11837,V,30000,DVBS2"
	TP56="11853,H,25000,DVBS2"
	TP58="11880,H,20000,DVBS2"
	TP59="11883,V,4800,DVBS"
	TP61="11916,V,30000,DVBS"
	TP62="11958,V,27500,DVBS"
	TP63="11977,H,27500,DVBS"
	TP65="11999,V,11666,DVBS2"
	TP66="12009,V,4444,DVBS2"
	TP67="12015,H,27500,DVBS"
	TP68="12034,V,27500,DVBS"
	TP69="12054,H,27500,DVBS"
	TP70="12073,V,27500,DVBS2"
	TP71="12083,H,13750,DVBS2"
	TP72="12095,H,4800,DVBS"
	TP73="12103,H,8333,DVBS2"
	TP74="12130,V,27500,DVBS"
	TP75="12188,V,27500,DVBS2"
	TP77="12196,H,8888,DVBS"
	TP79="12209,H,10000,DVBS2"
	TP80="12213,V,5833,DVBS2"
	TP81="12220,H,6500,DVBS"
	TP83="12228,V,8400,DVBS"
	TP85="12245,H,27500,DVBS2"
	TP86="12265,V,27500,DVBS2"
	TP87="12298,V,4800,DVBS"
	TP88="12311,V,15000,DVBS2"
	TP89="12329,H,6666,DVBS2"
	TP90="12336,H,5520,DVBS"
	TP91="12344,V,30000,DVBS"
	TP92="12346,H,9600,DVBS"
	TP93="12356,H,7100,DVBS2"
	TP94="12380,H,30000,DVBS"
	TP95="12380,V,27500,DVBS"
	TP96="12422,H,30000,DVBS"
	TP97="12423,V,27500,DVBS"
	TP100="12458,V,30000,DVBS"
	TP111="12605,V,34285,DVBS2"
	TP112="12610,H,20830,DVBS"
	TP118="12658,V,2222,DVBS"
	TP119="12685,H,27500,DVBS2"
	TP120="12685,V,30000,DVBS2"
	TP122="12729,V,30000,DVBS2"

	# Loop through each TPx variable
	for tp_var in "${!TP@}"; do
		tp_value="${!tp_var}"

		# Parse the TPx value
		if [[ $tp_value =~ ^([0-9]+),([VH]),([0-9]+),([^,]+)$ ]]; then
			FREQ="${BASH_REMATCH[1]}"
			POL="${BASH_REMATCH[2]}"
			SYMRATE="${BASH_REMATCH[3]}"
			DVBTYPE="${BASH_REMATCH[4]}"

			printf "\nFrequency: $FREQ, Polarity: $POL, Symbol Rate: $SYMRATE, DVB Type: $DVBTYPE\n"
			#set -x
			has_tp=$(echo ${tplist} | jq -r '.result | fromjson.data[] | select(.frequency == '${FREQ}' and .polarity == '\"$POL\"' and .symbol_rate == '${SYMRATE}') | .frequency')
			#set +x
			if [[ -n ${has_tp} ]]; then
				printf "Database already has this TP...\n"
			else
				printf "DB does NOT have this TP, adding...\n"
				
				add_tp
			fi
			
			tp_scan
			unselect_tp

		fi
	done
}


function watch_TV_RADIO() {
	
	tvradio=$1

	# First check if we have any channel in the list

	numofch=$(get_number_of_channels)
        if [ "${numofch}" -lt 1 ]
        then
                printf "No channels in DB!!!\n"
                exit 1
        fi

        printf "Preparing Channel List..."
        # Define the base JSON request
	#set -x
        base_request='{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getListOfServicesByIndex", "json": ["cur",'\"${tvradio}\"','
	#set +x

        offset=0
        increment=25

        services=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d "$base_request$offset,$increment]}}" | jq '.result | fromjson.data')
        offset=$((offset+increment))

        while [[ $offset -lt $numofch ]]
        do
                printf "."
                # Make the curl request and append the result to the services variable
                curservices=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d "$base_request$offset,$increment]}}" | jq '.result | fromjson.data')
                services=$(echo $services $curservices | jq -s add)
                offset=$((offset+increment))
        done

	printf "\nChannel list is created...\n"
	#services=$(construct_services_array) #Previously I was using this function but I gave up because below 20 lines can display a small progress when constrocting more than 500channels..

	# Check if the result field is an empty string or contains an error message
	is_valid_json=$(echo "$services" | jq -e . >/dev/null 2>&1 ; echo ${PIPESTATUS[1]})
	if [ "${is_valid_json}" == "1" ] 
	then
		printf "\n\n************\nNo channels, please SCAN first.\n*************\n\n\n"
		printf "\n\n************\nNo VALID JSON, system ERROR\n*************\n\n\n"
	elif [ "${is_valid_json}" == "4" ]
	then
		printf "\n\n************\nNo VALID JSON, system ERROR\n*************\n\n\n"
		printf "\n\n************\nNo channels, please SCAN first.\n*************\n\n\n"
	else
		while [ 1 ]; do

			printf "\nCHANNEL LIST\n"
			# Parse the JSON and print the names and URIs
			list_channels
			#curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getListOfServicesByIndex", "json": ["cur","all",0,50] }}' | jq -c '.result | fromjson.data[]' | jq -r '"\(input_line_number)) \(.name) ** \(.transponder) ** \(.sig_name)"' | column -t -s '**'
			printf "\nEnter q or Q to back to main menu..\n\n"
			read -p "Select a channel (enter ch number) or back to main menu (enter \"q\" ): " channel_choice
			printf "Choice is %s\n" ${channel_choice}

			if [ $channel_choice == "q" ] || [ $channel_choice == "Q" ]; then
				printf "Returning back to main menu...\n"
				return
			fi

			# Get the URI based on the selected channel name
			selected_uri=$(echo "$services" | jq -r --argjson choice "$channel_choice" '.[$choice-1] | .uri')

			printf "\nSelected URI is $selected_uri\n"

			stop_player
			prepare_display

			# Command to start watching the selected channel
			printf "\nStart playing ${selected_uri}...\n"
			curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Player.play", "json": [0,"'"$selected_uri"'",false,false,0,"",""] }}'
			printf "\n\n"
		done
	fi

}

function easy_scan(){

	#this function will scan some well-known TPs for test

	# First unselect current tp
	unselect_tp

	#ask user which TP to scan
	printf "\n"
	echo "Select an option:"
	echo "1) Scan Turksat TRT4K - 11767 V 15000 DVBS2"
	echo "2) Scan Turksat TRT-GROUP - 11054 V 30000 DVBS2"
	echo "3) Scan Turksat HABERTURK - 12209 H 10000 DVBS2"
	echo "4) Scan Turksat BEYAZTVHD - 12380 V 27500 DVBS"
	echo "5) Scan Turksat LOW-H - 11012 H 30000 DVBS2"

	printf "\n"
	read -p "Enter your choice: " choice

	case $choice in
		1)			
			printf "Scan TRT4K...\n"
			FREQ="11767"
			POL="V"
			SYMRATE="15000"
			DVBTYPE="DVBS2"
			;;	
		2)
			printf "Scan TRT GROUP...\n"
			FREQ="11054"
			POL="V"
			SYMRATE="30000"
			DVBTYPE="DVBS2"
			;;

		3)
			printf "Scan HABERTURK...\n"
			FREQ="12209"
			POL="H"
			SYMRATE="10000"
			DVBTYPE="DVBS2"
			;;
		4)
			printf "Scan BEYAZTV...\n"
			FREQ="12380"
			POL="V"
			SYMRATE="27500"
			DVBTYPE="DVBS"
			;;
		5)
			printf "Scan Turksat LOW-Horizontal...\n"
			FREQ="11012"
			POL="H"
			SYMRATE="30000"
			DVBTYPE="DVBS2"
			;;

		*)
			printf "wrong input! do nothing...\n"
			return
			;;
	esac
	
	add_tp
	tp_scan


}

function blind_scan(){

	printf "\nStarting blind scan, this will take around 30minutes...\n"

	# First unselect current tp
	unselect_tp

	#stop play, which includes stop scanning and stop tuning....
	stop_scan

	printf "\nStarting blind scan...\n"
	set -x
	#curl -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.startSearchEx", "json": ["blind", false, "{\"lnblist\":[{\"lnb\":\"'${selected_lnb}'\"}]}"] }}'
	curl -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.startSearchEx", "json": ["blind", false, "{\"lnblist\":[{\"lnb\":'${selected_lnb}'}]}"] }}'

	set +x

	count=10
	while [[ $count -gt 0 ]]
	do
		get_scan_status
		get_signal_level
		count=$((count-1))
		sleep 3
	done

}

function nit_scan(){

	unselect_tp
	#Add NIT TP
	FREQ="12423"
	POL="H"
	SYMRATE="30000"
	DVBTYPE="DVBS"
	add_tp
	
	stop_player


	# Select our TP
	printf "\nSelect our TP: %s %s %s ...\n" "${FREQ}" "${POL}" "${SYMRATE}"
	curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.selectTransponder", "json": [ "'$SATNAME'", "'${FREQ}${POL}${SYMRATE}'", true ] }}'


	# Scan
	printf "\nScan our TP...\n"
	curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.startSearchEx", "json": ["transponder", true, "{\"lnblist\":[{\"lnb\":\"'${selected_lnb}'\"}]}"] }}'

	# Wait for 10 seconds
	#printf "\nSleep 3 seconds...\n"
	sleep 2

	# Stop Scan
	printf "\nStop Scanning...\n"
	stop_scan


	count=10
	while [[ $count -gt 0 ]]
	do
		get_scan_status
		printf "\nGetting list of services...\n"
		count=$((count-1))
		sleep 3
		list_channels
		#curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getListOfServicesByIndex", "json": ["cur","all",0,50] }}' | jq -c '.result | fromjson.data[]' | jq -r '"\(input_line_number)) \(.name) ** \(.transponder) ** \(.sig_name)"' | column -t -s '**'
	done
	printf "\nNIT Scan will take at least 10 minutes, please stand by...\n"
}

function get_signal_level(){
	
	printf "\n"
	#sleep 3
	scan_status="true"
	while [ "${scan_status}" == "true" ]
	do
		scan_status=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getStatus", "json": [] }}' | jq -c '.result | fromjson.data.started')
		curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getFrontend", "json": [0] }}' | jq -c '.result | fromjson.data' | jq -r '"Quality ** Strength ** TUNE \n \(.integrity) ** \(.strength) ** \(.tune_Status)"' | column -t -s '**'
		get_progress=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getStatus", "json": [] }}' | jq -c '.result | fromjson.data.progress')
		if [ "${get_progress}" == "100" ]
		then
			printf "\nScanning COMPLETED\n"
			break
		fi
	done

}

function get_scan_status(){
	
	printf "\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getStatus", "json": [] }}' | jq -c '.result | fromjson.data' | jq -r '"progress ** scanning \n \(.progress) ** \(.started)"' | column -t -s '**'

}

#First check if DTVkit system service is ready or not
is_dtvkit_ready

prepare_database

if [ $# -eq 4 ]; then
	FREQ=$1
	POL=$2
	SYMRATE=$3
	DVBTYPE=$4

	add_tp
	tp_scan
fi



while true; do
	printf "\n"
	echo "Select an option:"
	echo "1) Scan same TP again"
	echo "2) Scan another TP"
	echo "3) Stop Scanning"
	echo "4) Watch TV"
	echo "5) Stop Watching"
	echo "6) Delete All satellite, all LNBs and all channels"
	echo "7) Back to RDK Launcher"
	echo "8) Play 4K Test Video"
	echo "9) Play 1080p Test Video"
	echo "10) EXIT"
	echo "11) Enable Tuner Debug"
	echo "12) Disable Tuner Debug"
	echo "13) Disable Deinterlacer"
	echo "14) Change Framerate"
	echo "15) EASY SCAN"
	echo "16) BLIND SCAN"
	echo "17) SAT SCAN"
	echo "18) NIT SCAN"
	echo "19) Listen Radio"
	echo "20) Dolby Vision Test"
	echo "21) Dolby Audio Test"
	echo "22) Dolby ATMOS Test"

	printf "\n"
	read -r -p "Enter your choice: " choice

	case $choice in
		1)
			tp_scan
			;;
		2)
			unselect_tp
			update_tp_info
			add_tp
			tp_scan
			;;
		3)
			echo "Stopping Scanning..."
			stop_scan
			;;
		4)
			watch_TV_RADIO tv
			;;

		5)
			echo "Stopping Watching..."
			# Command to stop watching TV
			stop_player
			;;
		6)
			echo "Deleting All channels..."
			# Command to delete all channels
			stop_player
			delete_all_satellites
			delete_all_lnbs
			rm -rf /data/dtvkit.sqlite3
			rm -rf /data/persistent/dtvkit/dtvkit.sqlite3
			systemctl restart dtvkit
			printf "\nPlease wait 10 seconds to restart services...\n"
			sleep 10
			prepare_database
			;;
		7)
			echo "Back to RDK Launcher"
			stop_player
			back_to_launcher
			;;
		8)
			printf "Play 4K Test Video..."
			printf "Press Q on your keyboard to stop playback !!!\n"
			sleep 1
			stop_player
			prepare_display
			gst-play-1.0 https://dash.akamaized.net/akamai/4k/Kluge.mpd
			;;
		9)
			echo "Play 1080p Test Video"
			printf "Press Q on your keyboard to stop playback !!!\n"
			stop_player
			prepare_display
			gst-play-1.0 http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4
			;;
		10)
			echo "Exiting..."
			exit 0
			;;
		11)
			printf "Enabling tuner debug...\n"
			enable_tuner_debug
			;;
		12)
			printf "Disabling tuner debug...\n"
			disable_tuner_debug
			;;
		13)
			printf "Disabling deinterlacer...\n"
			disable_deinterlacer
			;;
		14)
			printf "Modify framerate..\n"
			set_resolution_framerate
			;;
		15)
			easy_scan
			;;
		16)
			blind_scan
			;;
		17)
			satscan
			;;
		18)	
			nit_scan
			;;
		19)
			watch_TV_RADIO rad
			;;
		20)
			echo "Dolby Vision test"
			printf "Press Q on your keyboard to stop playback !!!\n"
			stop_player
			prepare_display
			gst-play-1.0 http://kernelmax.com:9000/dva.mp4
			;;
		21)
			echo "Dolby Audio test"
			printf "Press Q on your keyboard to stop playback !!!\n"
			stop_player
			prepare_display
			gst-play-1.0 http://kernelmax.com:9000/dolby-audio.mp4
			;;
		22)
			echo "Dolby ATMOS test"
			printf "Press Q on your keyboard to stop playback !!!\n"
			stop_player
			prepare_display
			gst-play-1.0 http://kernelmax.com:9000/Universe_Fury2.mp4
			;;

		*)
			echo "Invalid option. Please choose a valid option (1-9)."
			;;
	esac
done
