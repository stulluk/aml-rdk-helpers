#!/usr/bin/env bash

for file in $(ls /sys/bus/iio/devices/iio\:device0/in_voltage*_input)
do
	echo "reading $file"
	printf "Value is %d\n" "$(cat $file)"
done

