#!/usr/bin/env bash

#set -x

DEV_IP_PORT="127.0.0.1:9998"
SATNAME="TestSAT"

#Diseqc1.0 Port number
DISEQC1_0_PORT=1

#Diseqc1.1 Port number
DISEQC1_1_PORT=1

function is_dtvkit_ready(){

	if systemctl is-active --quiet  dtvkit
	then
		printf "DTVkit service is ready, starting..\n"
	else
		printf "DTVkit service is NOT ready, please wait a while and re-run this script..\n"
		exit 1
	fi

}

function print_oneliner_usage(){

	printf "Error! Usage : %s freq polarity symrate DVBS\n" "$(basename "${0}")"
	printf "Example : %s 12685 V 27500 DVBS2 \n" "$(basename "${0}")"

}

function disable_deinterlacer() {

	echo 0 > /sys/module/decoder_common/parameters/v4lvideo_add_di
	echo 45 0x100000 > /sys/kernel/debug/di_top/mw_di

}

function enable_tuner_debug() {

	#enable debug commands
	echo 0xfff > /sys/module/aml_media/parameters/aml_demod_debug
	echo diseqc_dbg 255 > /sys/class/dtvdemod/attr

}

function disable_tuner_debug() {

	echo 0x0 > /sys/module/aml_media/parameters/aml_demod_debug
	echo diseqc_dbg 0 > /sys/class/dtvdemod/attr

}

function set_resolution_framerate(){

	# Enable debug mode otherwise can not select a new framerate
	echo "1" > /sys/class/display/debug
	MODE="/sys/class/display/mode"

	printf "\nResolutions: \n"
	echo "1) 1080p50Hz"
	echo "2) 1080p60Hz"
	echo "3) 2160p50Hz"
	echo "4) 2160p60Hz"

	printf "\n"
	read -r -p "Select resolutions:" RESFRM
	case ${RESFRM} in
		1)
			echo "1080p50hz" > ${MODE}
			;;
		2)
			echo "1080p60hz" > ${MODE}
			;;
		3)
			echo "2160p50hz" > ${MODE}
			;;
		4)
			echo "2160p60hz" > ${MODE}
			;;
		*)
			printf "Wrong input, doing nothing!!!...\n"
			;;
	esac


}

function update_tp_info() {

	read -r -p "Enter frequency in Mhz ( such as 12685): " FREQ
	read -r -p "Enter polarity ( V or H ): " POL
	read -r -p "Enter Symbol Rate ( Such as 27500) : " SYMRATE
	read -r -p "Enter DVBS/S2 type (Such as DVBS or DVBS2 ) : " DVBTYPE

}

function prepare_display()
{
	#Prepare environment
	echo 1 > /sys/kernel/debug/dri/0/vpu/blank

	printf "\nCreating Display\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.createDisplay", "params": { "client": "test-0", "displayName": "test-0" }}'
	printf "\nMoving Display to Front\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.moveToFront", "params": { "client": "test-0" }}'
	printf "\nSetting Focus to Display\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.setFocus", "params": { "client": "test-0" }}'

	export XDG_RUNTIME_DIR=/run
	export WESTEROS_GL_GRAPHICS_MAX_SIZE=3840x2160
	# # USE_DMABUF is needed
	export WESTEROS_SINK_AMLOGIC_USE_DMABUF=1
	export PLAYREADY_OP_DIGITAL_LEVEL=200
	export WESTEROS_GL_USE_REFRESH_LOCK=1
	export WESTEROS_SINK_USE_FREERUN=1
	export WESTEROS_GL_USE_AMLOGIC_AVSYNC=1
	export WESTEROS_SINK_USE_ESSRMGR=1
	export LD_PRELOAD=libwesteros_gl.so.0.0.0
	export WAYLAND_DISPLAY=test-0
}

function back_to_launcher() {
	# After test, recover OSD
	echo 0 > /sys/kernel/debug/dri/0/vpu/blank
	# recover OSD focus
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.setFocus", "params": { "client": "residentapp" }}'
}

function stop_scan() {
	printf "\nStop Scanning...\n"
	for tuner in Dvbs Dvbt Dvbc
	do
		stop_scan_result=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "'${tuner}'.finishSearch", "json": [ true ]}}' | jq -c '.result | fromjson.data')
		if [ "${stop_scan_result}" == "false" ]
		then
			printf "\n ERROR !!! Failed to Stop Scan!\n"
		fi
	done
	printf "\n"

	#curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbt.finishSearch", "json": [ true ]}}'
	#printf "\n"
	#curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbc.finishSearch", "json": [ true ]}}'
	#printf "\n"
}

function stop_tune() {
	printf "\nStop Tuning..\n"
	stop_tune_result=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.tuneActionStop", "json": [ ] }}' | jq -c '.result | fromjson.data')
	if [ "${stop_tune_result}" == "false" ]
	then
			printf "\n ERROR !!! Failed to Stop Tuning!\n"
	fi

}

function stop_player() {

	#First stop scanning
	stop_scan
	#stop tuning
	stop_tune
	#Now stop player
	printf "\nStop Player...\n"

	stop_player_result=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Player.stop", "json": [ ] }}' | jq -c '.result | fromjson.data')
	if [ "${stop_player_result}" == "false" ]
	then
			printf "\n ERROR !!! Failed to Stop Player!\n"
	fi
	printf "\n"

}

function get_satellite_list() {

	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getSatellites", "json": [""] }}' | jq -r '.result | fromjson | .data[].name'

}

function delete_single_sat() {

	satname="$1"

	newsatname=$(echo "${satname}" | sed 's/\*\*/  /g ; s/\*/ /g')

	printf "SINGLE DELETE: We will delete %s\n" "${newsatname}"

	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d "{\"jsonrpc\": \"2.0\",\"id\": 1,\"method\": \"DTV.1.Invoke\", \"params\": { \"command\": \"Dvbs.deleteSatellite\", \"json\": [\"${newsatname}\"] }}" | jq

}



function delete_all_satellites() {

	get_satellite_list > tempsatlist.txt

	while read -r satellite; do
		printf "%s\n" "${satellite}"
		if [ "${satellite}" != "Turksat2/3/4A" ]; then
			printf "\n%s will be deleted\n" "${satellite}"

			#rename the satellite name so that double spaces will be handled
			newsatname=$(printf "%s" "${satellite}" | sed 's/  /\*\*/g ; s/ /\*/g')
			printf "we will delete %s\n" "${newsatname}"
			delete_single_sat "${newsatname}"
		fi
	done < tempsatlist.txt

	#delete telstar 12 seperately
	printf "\nDeleting telstar..\n"
	curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.deleteSatellite", "json": ["Telstar 12 "] }}'

}

function delete_all_lnbs() {

	for lnb_num in $(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getLnbs", "json": [ ] }}' | jq -r '.result | fromjson.data[] | .lnb'); do
		printf "\nDeleting lnb %d\n" "${lnb_num}"
		curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.deleteLnb", "json": [ "'${lnb_num}'" ] }}'
	done

}

function select_lnb() {

	#set -x
	local json_data="$1"
	local selected_lnb=$(echo "$json_data" | jq -r '.result | fromjson.data[] | select(.low_local_oscillator_frequency == 9750 and .high_local_oscillator_frequency == 10600 and .c_switch == '$DISEQC1_0_PORT' and .u_switch == '$DISEQC1_1_PORT' ) | .lnb' | head -n1)
	#local selected_lnb=$(echo "$json_data" | jq -r '.result | fromjson.data[] | select(.c_switch == '$DISEQC1_0_PORT' and .u_switch == '$DISEQC1_1_PORT' ) | .lnb' | head -n1)
	echo "$selected_lnb"
	#set +x
}

function unselect_tp() {

	#First check if there was a TP defined before or not

	if [ -z ${FREQ} ]
	then
		printf "You never searched for a TP before, not unselecting any TP...\n"
		return
	fi

	# Un-Select current TP
	printf "\nUn-Select our TP: %s %s %s ...\n" "${FREQ}" "${POL}" "${SYMRATE}"
	curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.selectTransponder", "json": [ "'$SATNAME'", "'${FREQ}${POL}${SYMRATE}'", false ] }}'
	printf "\n\n"
}




function construct_services_array(){

	numofch=$(get_number_of_channels)
	if [ "${numofch}" -lt 1 ]
	then
		printf "No channels in DB!!!\n"
		printf "Please scan some channels first!!!\n"
		exit 1
	fi

	# Define the base JSON request
	base_request='{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getListOfServicesByIndex", "json": ["all","all",'

	offset=0
	increment=25

	services=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d "$base_request$offset,$increment]}}" | tr -d '\n' | jq '.result | fromjson.data')
	offset=$((offset+increment))

	while [[ $offset -lt $numofch ]]
	do
		# Make the curl request and append the result to the services variable
		curservices=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d "$base_request$offset,$increment]}}" | tr -d '\n' | jq '.result | fromjson.data')
		services=$(echo $services $curservices | jq -s add)
		offset=$((offset+increment))
	done

	printf "\n"
	# Print the combined JSON response
	echo "$services"


}

function get_number_of_channels(){

	numofch=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getNumberOfServices", "json": [] }}' | jq -r '.result | fromjson.data')
	echo $numofch
}

function list_channels() {

	numofch=$(get_number_of_channels)
	if [ "${numofch}" -lt 1 ]
	then
		printf "No channels in DB!!!\n"
		printf "Please Scan some channels first!!\n"
		sleep 2
		return
	fi
	printf "\n*******************************************\n"
	printf "Number of Channel in DB is %d\n" "${numofch}"
	printf "*******************************************\n"

	#Lets read whole CHLIST chunk-by-chunk, 50 channels at a time
	offset=0
	increment=25

	while [[ $offset -lt $numofch ]]
	do
		#set -x
		curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getListOfServicesByIndex", "json": ["all","all",'$offset','$increment'] }}' | tr -d '\n' | jq -c '.result | fromjson.data[]' | jq -r '"\(input_line_number+'$offset')) \(.name) ** \(.transponder) ** \(.freq/1000000) ** \(.sig_name)"' | column -t -s '**'
		#set +x
		offset=$((offset+increment))
	done
}

function prepare_database() {

	stop_player
	
	# Disable automatic channel list ordering via LCN
	printf "Disable automatic channel list ordering via LCN..\n"
	disable_auto_lcn_result=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.SetAutomaticOrderingEnabled", "json": [ false ] }}' | jq -c '.result | fromjson.data')
	if [ "${disable_auto_lcn_result}" == "false" ]
	then
		printf "\n ERROR! Couldn't disable Auto LCN ordering!\n"
	fi

	#First check if our Universal LNB exist in Database or not
	json_response=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getLnbs", "json": [ ] }}')

	selected_lnb=$(select_lnb "$json_response")

	if [[ -n "$selected_lnb" ]]; then
		printf "\nSelected LNB: $selected_lnb"
	else
		printf "\nNo LNB found with high_local_oscillator_frequency=10600"

		#lets add our own Universal LNB with Diseqc1.0 port = 1 and Diseqc1.1 port = 1
		printf "\nLets add our own Universal LNB with Diseqc1.0 port = %d and Diseqc1.1 port = %d..\n" "${DISEQC1_0_PORT}" "${DISEQC1_1_PORT}"

		curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.addLnb", "json": [ false, 0, 0, false, "none", '$DISEQC1_0_PORT', '$DISEQC1_1_PORT', 0, 1, 10700, 11700, 9750, "on", "auto", 11700, 12750, 10600, "on", "auto" ] }}'

		#wait a while to let dtvkitserver to be stable
		sleep 2
		json_response=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getLnbs", "json": [ ] }}')

		selected_lnb=$(select_lnb "$json_response")
		if [ -n "$selected_lnb" ]; then
			printf "\nSelected LNB: $selected_lnb\n"
		else
			printf "Adding our Universal LNB failed! System ERROR!!\n"
			exit 1
		fi
	fi

	# if our TestSAT doesn't exist, add it
	num_testsat=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getSatellites", "json": [""] }}' | jq -r '.result | fromjson | .data[].name' | grep -c "${SATNAME}")

	if [ ${num_testsat} -ne 1 ]
	then
		printf "\n%s does NOT exit in DB, adding it...\n" "${SATNAME}"
		# Add our SATELLITE for selected LNB
		printf "\nAdding $SATNAME...\n"
		curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.addSatellite", "json": [ "'$SATNAME'", false, 42, 15, "'$selected_lnb'" ] }}'
	fi

}

function add_tp() {
	# Add our TP
	printf "\nAdding our TP...\n"
	curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.addTransponder", "json": [ "'$SATNAME'", '$FREQ', "'$POL'", '$SYMRATE', "'$DVBTYPE'", "auto", "auto", "auto" ] }}'
}

function tp_scan() {

	#First check if there was a TP defined before or not

	if [ -z ${FREQ} ]
	then
		printf "You never searched for a TP before, please use 2nd option below to scan a TP...\n"
		return
	fi

	stop_player

	# Select our TP
	printf "\nSelect our TP: %s %s %s ...\n" "${FREQ}" "${POL}" "${SYMRATE}"
	curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.selectTransponder", "json": [ "'$SATNAME'", "'${FREQ}${POL}${SYMRATE}'", true ] }}'

	# Scan

	printf "\nScan our TP...\n"
	curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.startSearchEx", "json": ["transponder", false, "{\"lnblist\":[{\"lnb\":\"'${selected_lnb}'\"}]}"] }}'


	get_scan_status DVB-S
	sleep 2
	get_signal_level DVB-S
	get_scan_status DVB-S

	# Wait for 1 seconds
	printf "\nSleep 1 seconds...\n"
	sleep 1

	# Stop Scan
	printf "\nStop Scanning...\n"
	stop_scan

	# List Channels
	#printf "\nList channels...\n"
	#list_channels
	
	printf "\n*******************************************\n"
	printf "Number of Channels in DB: %d\n" "$(get_number_of_channels)"
	printf "*******************************************\n"

}

print_number_of_channels(){
	
	printf "\n*******************************************\n"
	printf "Number of Channels in DB: %d\n" "$(get_number_of_channels)"
	printf "*******************************************\n"

}

satscan() {

	#first get tplist from system
	tplist=$(curl -s -X POST http://"${DEV_IP_PORT}"/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.getTransponders", "json": [ "'$SATNAME'" ] }}')

	# Statically declare Turksat TP values
	TP3="10958,V,6250,DVBS2"
	TP11="11012,H,30000,DVBS2"
	TP17="11053,H,8000,DVBS"
	TP18="11054,V,30000,DVBS2"
	TP22="11096,H,30000,DVBS"
	TP33="11180,H,29750,DVBS"
	TP35="11265,V,30000,DVBS2"
	TP36="11305,V,30000,DVBS2"
	TP48="11558,V,30000,DVBS"
	TP50="11676,V,24444,DVBS"
	TP51="11767,V,15000,DVBS2"
	TP52="11789,H,20000,DVBS2"
	TP53="11795,V,30000,DVBS2"
	TP54="11822,H,14166,DVBS2"
	TP55="11837,V,30000,DVBS2"
	TP56="11853,H,25000,DVBS2"
	TP58="11880,H,20000,DVBS2"
	TP59="11883,V,4800,DVBS"
	TP61="11916,V,30000,DVBS"
	TP62="11958,V,27500,DVBS"
	TP63="11977,H,27500,DVBS"
	TP65="11999,V,11666,DVBS2"
	TP66="12009,V,4444,DVBS2"
	TP67="12015,H,27500,DVBS"
	TP68="12034,V,27500,DVBS"
	TP69="12054,H,27500,DVBS"
	TP70="12073,V,27500,DVBS2"
	TP71="12083,H,13750,DVBS2"
	TP72="12095,H,4800,DVBS"
	TP73="12103,H,8333,DVBS2"
	TP74="12130,V,27500,DVBS"
	TP75="12188,V,27500,DVBS2"
	TP77="12196,H,8888,DVBS"
	TP79="12209,H,10000,DVBS2"
	TP80="12213,V,5833,DVBS2"
	TP81="12220,H,6500,DVBS"
	TP83="12228,V,8400,DVBS"
	TP85="12245,H,27500,DVBS2"
	TP86="12265,V,27500,DVBS2"
	TP87="12298,V,4800,DVBS"
	TP88="12311,V,15000,DVBS2"
	TP89="12329,H,6666,DVBS2"
	TP90="12336,H,5520,DVBS"
	TP91="12344,V,30000,DVBS"
	TP92="12346,H,9600,DVBS"
	TP93="12356,H,7100,DVBS2"
	TP94="12380,H,30000,DVBS"
	TP95="12380,V,27500,DVBS"
	TP96="12422,H,30000,DVBS"
	TP97="12423,V,27500,DVBS"
	TP100="12458,V,30000,DVBS"
	TP111="12605,V,34285,DVBS2"
	TP112="12610,H,20830,DVBS"
	TP118="12658,V,2222,DVBS"
	TP119="12685,H,27500,DVBS2"
	TP120="12685,V,30000,DVBS2"
	TP122="12729,V,30000,DVBS2"

	# Loop through each TPx variable
	for tp_var in "${!TP@}"; do
		tp_value="${!tp_var}"

		# Parse the TPx value
		if [[ $tp_value =~ ^([0-9]+),([VH]),([0-9]+),([^,]+)$ ]]; then
			FREQ="${BASH_REMATCH[1]}"
			POL="${BASH_REMATCH[2]}"
			SYMRATE="${BASH_REMATCH[3]}"
			DVBTYPE="${BASH_REMATCH[4]}"

			printf "\nFrequency: $FREQ, Polarity: $POL, Symbol Rate: $SYMRATE, DVB Type: $DVBTYPE\n"
			#set -x
			has_tp=$(echo ${tplist} | jq -r '.result | fromjson.data[] | select(.frequency == '${FREQ}' and .polarity == '\"$POL\"' and .symbol_rate == '${SYMRATE}') | .frequency')
			#set +x
			if [[ -n ${has_tp} ]]; then
				printf "Database already has this TP...\n"
			else
				printf "DB does NOT have this TP, adding...\n"

				add_tp
			fi

			tp_scan
			unselect_tp

		fi
	done
}


function watch_TV_RADIO() {

	tvradio=$1

	# First check if we have any channel in the list

	numofch=$(get_number_of_channels)
	if [ "${numofch}" -lt 1 ]
	then
		printf "No channels in DB!!!\n"
		printf "Please scan some channels first!!!\n"
		sleep 2
		return
	fi

	printf "Preparing Channel List..."
	# Define the base JSON request
	#set -x
	base_request='{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getListOfServicesByIndex", "json": ["all",'\"${tvradio}\"','
	#set +x

	offset=0
	increment=25

	services=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d "$base_request$offset,$increment]}}" | tr -d '\n' | jq '.result | fromjson.data')
	offset=$((offset+increment))

	while [[ $offset -lt $numofch ]]
	do
		printf "."
		# Make the curl request and append the result to the services variable
		curservices=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d "$base_request$offset,$increment]}}" | tr -d '\n' | jq '.result | fromjson.data')
		services=$(echo $services $curservices | jq -s add)
		offset=$((offset+increment))
	done

	printf "\nChannel list is created...\n"
	#services=$(construct_services_array) #Previously I was using this function but I gave up because below 20 lines can display a small progress when constrocting more than 500channels..

	# Check if the result field is an empty string or contains an error message
	is_valid_json=$(echo "$services" | jq -e . >/dev/null 2>&1 ; echo ${PIPESTATUS[1]})
	if [ "${is_valid_json}" == "1" ] 
	then
		printf "\n\n************\nNo channels, please SCAN first.\n*************\n\n\n"
		printf "\n\n************\nNo VALID JSON, system ERROR\n*************\n\n\n"
	elif [ "${is_valid_json}" == "4" ]
	then
		printf "\n\n************\nNo VALID JSON, system ERROR\n*************\n\n\n"
		printf "\n\n************\nNo channels, please SCAN first.\n*************\n\n\n"
	else
		while [ 1 ]; do

			printf "\nCHANNEL LIST\n"
			# Parse the JSON and print the names and URIs
			list_channels
			printf "\nEnter q or Q to back to main menu..\n\n"
			printf "\nPress A or a to select audio track\n"
			printf "\nPress L or l to select audio language\n"
			read -r -p "Select a channel (enter ch number) or back to main menu (enter \"q\" ): " channel_choice
			printf "Choice is %s\n" ${channel_choice}
				
			if [ $channel_choice == "l" ] || [ $channel_choice == "L" ]; then
				printf "Starting Audio Language Selection Menu...\n"
				change_audio_language primary
				continue
			fi

			if [ $channel_choice == "a" ] || [ $channel_choice == "A" ]; then
				printf "Starting Audio Track Selection Menu...\n"
				change_audio_track
				continue
			fi

			if [ $channel_choice == "q" ] || [ $channel_choice == "Q" ]; then
				printf "Returning back to main menu...\n"
				return
			fi

			# Get the URI based on the selected channel name
			selected_uri=$(echo "$services" | jq -r --argjson choice "$channel_choice" '.[$choice-1] | .uri')

			printf "\nSelected URI is $selected_uri\n"

			stop_player
			prepare_display

			# Command to start watching the selected channel
			printf "\nStart playing ${selected_uri}...\n"
			curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Player.play", "json": [0,"'"$selected_uri"'",false,false,0,"",""] }}'
			printf "\n\n"
		done
	fi

}

function easy_scan(){

	#this function will scan some well-known TPs for test

	# First unselect current tp
	unselect_tp

	#ask user which TP to scan
	printf "\n"
	echo "Select an option:"
	echo "1) Scan Turksat TRT4K - 11767 V 15000 DVBS2"
	echo "2) Scan Turksat TRT-GROUP - 11054 V 30000 DVBS2"
	echo "3) Scan Turksat HABERTURK - 12209 H 10000 DVBS2"
	echo "4) Scan Turksat BEYAZTVHD - 12380 V 27500 DVBS"
	echo "5) Scan Turksat LOW-H - 11012 H 30000 DVBS2"
	echo "6) BACK to Main Menu"

	printf "\n"
	read -r -p "Enter your choice: " choice

	case $choice in
		1)			
			printf "Scan TRT4K...\n"
			FREQ="11767"
			POL="V"
			SYMRATE="15000"
			DVBTYPE="DVBS2"
			;;	
		2)
			printf "Scan TRT GROUP...\n"
			FREQ="11054"
			POL="V"
			SYMRATE="30000"
			DVBTYPE="DVBS2"
			;;

		3)
			printf "Scan HABERTURK...\n"
			FREQ="12209"
			POL="H"
			SYMRATE="10000"
			DVBTYPE="DVBS2"
			;;
		4)
			printf "Scan BEYAZTV...\n"
			FREQ="12380"
			POL="V"
			SYMRATE="27500"
			DVBTYPE="DVBS"
			;;
		5)
			printf "Scan Turksat LOW-Horizontal...\n"
			FREQ="11012"
			POL="H"
			SYMRATE="30000"
			DVBTYPE="DVBS2"
			;;
		6)
			return
			;;

		*)
			printf "wrong input! do nothing...\n"
			return
			;;
	esac

	add_tp
	tp_scan


}

function blind_scan(){

	printf "\nStarting blind scan, this will take around 30minutes...\n"

	# First unselect current tp
	unselect_tp

	#stop play, which includes stop scanning and stop tuning....
	stop_scan

	printf "\nStarting blind scan...\n"
	#set -x
	curl -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.startSearchEx", "json": ["blind", false, "{\"lnblist\":[{\"lnb\":'${selected_lnb}'}]}"] }}'

	#set +x

	count=2
	while [[ $count -gt 0 ]]
	do
		get_scan_status DVB-S
		get_signal_level DVB-S
		count=$((count-1))
		sleep 3
	done

}

function nit_scan(){

	unselect_tp
	#Add NIT TP
	FREQ="12423"
	POL="H"
	SYMRATE="30000"
	DVBTYPE="DVBS"
	add_tp

	stop_player


	# Select our TP
	printf "\nSelect our TP: %s %s %s ...\n" "${FREQ}" "${POL}" "${SYMRATE}"
	curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.selectTransponder", "json": [ "'$SATNAME'", "'${FREQ}${POL}${SYMRATE}'", true ] }}'


	# Scan
	printf "\nScan our TP...\n"
	curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbs.startSearchEx", "json": ["transponder", true, "{\"lnblist\":[{\"lnb\":\"'${selected_lnb}'\"}]}"] }}'

	sleep 2

	# Stop Scan
	printf "\nStop Scanning...\n"
	stop_scan


	count=2
	while [[ $count -gt 0 ]]
	do
		get_scan_status DVB-S
		printf "\nGetting list of services...\n"
		count=$((count-1))
		sleep 3
		list_channels
	done
	printf "\nNIT Scan will take at least 10 minutes, please stand by...\n"
}

function get_signal_level(){

	case $1 in
		DVB-S)			
			tunertype="Dvbs"
			;;	
		DVB-T)			
			tunertype="Dvbt"
			;;	
		DVB-C)			
			tunertype="Dvbc"
			;;
		*)
			printf "wrong input! do nothing...\n"
			return
			;;
	esac

	printf "\n"
	sleep 3
	scan_status="true"
	while [ "${scan_status}" == "true" ]
	do
		scan_status=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "'$tunertype'.getStatus", "json": [] }}' | jq -c '.result | fromjson.data.started')
		#printf "scan_status is ${scan_status}\n"
		curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getFrontend", "json": [0] }}' | jq -c '.result | fromjson.data' | jq -r '"Quality ** Strength ** TUNE \n \(.integrity) ** \(.strength) ** \(.tune_Status)"' | column -t -s '**'
		get_progress=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "'$tunertype'.getStatus", "json": [] }}' | jq -c '.result | fromjson.data.progress')
		#printf "scan_progress is ${get_progress}\n"
		if [ "${get_progress}" == "100" ]
		then
			printf "\nScanning COMPLETED\n"
			break
		else
			printf "\nScanning...\n"
		fi
	done

}

function get_scan_status(){
	
	printf "\n"
	case $1 in
		DVB-S)			
			tunertype="Dvbs"
			;;	
		DVB-T)			
			tunertype="Dvbt"
			;;	
		DVB-C)			
			tunertype="Dvbc"
			;;
		*)
			printf "wrong input! do nothing...\n"
			return
			;;
	esac

	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "'$tunertype'.getStatus", "json": [] }}' | jq -c '.result | fromjson.data' | jq -r '"progress ** scanning \n \(.progress) ** \(.started)"' | column -t -s '**'

}

function automatic_dvbt_scan(){

	echo "Running Automatic DVB-T scan..."
	sleep 1
	# Command for Automatic DVBT scan
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbt.startSearch", "json": [true,false] }}'
	sleep 1
	get_signal_level DVB-T
}


function manual_dvbt_scan()
{
	# Display DVBT frequencies for each channel number
	printf "\n"
	printf "Channel Number - Frequency (MHz):\n"
	printf "2     - 50.5    3     - 57.5    4     - 64.5 \n"
	printf "5     - 177.5   6     - 184.5   7     - 191.5\n"
	printf "8     - 198.5   9     - 205.5   10    - 212.5\n"
	printf "11    - 219.5   12    - 226.5\n"

	channel_number=21
	while [ $channel_number -le 69 ]; do
		if [ $channel_number -le 61 ]; then
			frequency=$((474000000 + (channel_number - 21) * 8000000))
		else
			frequency=$((474000000 + (channel_number - 22) * 8000000))
		fi

		printf "%-5d - %-5d   " $channel_number $((frequency / 1000000))

		((channel_number++))
		if [ $((channel_number % 3)) -eq 0 ] && [ $channel_number -le 69 ]; then
			echo
		fi
	done

	printf "\n"

	read -r -p "Select a channel number (2-69): " channel_number

	if [ "$channel_number" -ge 2 ] && [ "$channel_number" -le 69 ]; then
		case $channel_number in
			2)
				frequency=50.5
				;;
			3)
				frequency=57.5
				;;
			4)
				frequency=64.5
				;;
			5)
				frequency=177.5
				;;
			6)
				frequency=184.5
				;;
			7)
				frequency=191.5
				;;
			8)
				frequency=198.5
				;;
			9)
				frequency=205.5
				;;
			10)
				frequency=212.5
				;;
			11)
				frequency=219.5
				;;
			12)
				frequency=226.5
				;;
			21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 | 32 | 33 | 34 | 35 | 36 | 37 | 38 | 39 | 40 | 41 | 42 | 43 | 44 | 45 | 46 | 47 | 48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 | 56 | 57 | 58 | 59 | 60 | 61 | 62 | 63 | 64 | 65 | 66 | 67 | 68 | 69)
				if [ $channel_number -le 61 ]; then
					frequency=$((474000000 + (channel_number - 21) * 8000000))
				else
					frequency=$((474000000 + (channel_number - 22) * 8000000))
				fi
				;;
			*)
				echo "Invalid channel number selection."
				exit 1
				;;
		esac
	else
		echo "Invalid channel number selection."
		exit 1
	fi

	# Ask for DVB-T type
	read -r -p "Select DVB-T Type: 
	1) DVB-T 
	2) DVB-T2
	Enter the corresponding number (1-2): " dvbt_type

	case $dvbt_type in
		1)
			dvbt_type="DVB-T"
			# Bandwidth can be one of 5Mhz, 6Mhz, 7Mhz, and 8Mhz
			read -r -p "Select Bandwidth:
			1) 5Mhz
			2) 6Mhz
			3) 7Mhz
			4) 8Mhz
			Enter the corresponding number (1-4): " bandwidth_option

			case $bandwidth_option in
				1)
					bandwidth="5Mhz"
					;;
				2)
					bandwidth="6Mhz"
					;;
				3)
					bandwidth="7Mhz"
					;;
				4)
					bandwidth="8Mhz"
					;;
				*)
					echo "Invalid bandwidth option. Exiting..."
					exit 1
					;;
			esac

		# FFT mode can be 2K, 4K, and 8K
		read -r -p "Select FFT Mode: 
		1) 2K 
		2) 4K 
		3) 8K
		Enter the corresponding number (1-3): " fft_option
		case $fft_option in
			1)
				fft_mode="2K"
				;;
			2)
				fft_mode="4K"
				;;
			3)
				fft_mode="8K"
				;;
			*)
				echo "Invalid FFT mode input. Exiting..."
				exit 1
				;;
		esac
		;;
	2)
		dvbt_type="DVB-T2"
		# Bandwidth can be one of 1.7Mhz, 5Mhz, 6Mhz, 7Mhz, 8Mhz, and 10Mhz
		read -r -p "Select Bandwidth:
		1) 1.7Mhz
		2) 5Mhz
		3) 6Mhz
		4) 7Mhz
		5) 8Mhz
		6) 10Mhz
		Enter the corresponding number (1-6): " bandwidth_option

		case $bandwidth_option in
			1)
				bandwidth="1.7Mhz"
				;;
			2)
				bandwidth="5Mhz"
				;;
			3)
				bandwidth="6Mhz"
				;;
			4)
				bandwidth="7Mhz"
				;;
			5)
				bandwidth="8Mhz"
				;;
			6)
				bandwidth="10Mhz"
				;;
			*)
				echo "Invalid bandwidth option. Exiting..."
				exit 1
				;;
		esac

		# FFT mode can be 1K, 2K, 4K, 8K, 16K, and 32K
		read -r -p "Select FFT Mode: 
		1) 1K
		2) 2K 
		3) 4K 
		4) 8K
		5) 16K
		6) 32K
		Enter the corresponding number (1-6): " fft_option
		case $fft_option in
			1)
				fft_mode="1K"
				;;
			2)
				fft_mode="2K"
				;;
			3)
				fft_mode="4K"
				;;
			4)
				fft_mode="8K"
				;;
			5)
				fft_mode="16K"
				;;
			6)
				fft_mode="32K"
				;;
			*)
				echo "Invalid FFT mode input. Exiting..."
				exit 1
				;;
		esac
		;;
	*)
		echo "Invalid DVB-T type input. Exiting..."
		exit 1
		;;
	esac

	echo "Running Manual DVBT scan..."
	# Command for Manual DVBT scan
	curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbt.startManualSearchByFreq", "json": [true,false,'"$frequency"',"'"$bandwidth"'","'"$fft_mode"'","'"$dvbt_type"'"] }}'
	get_signal_level DVB-T
}

function automatic_dvbc_scan(){

	echo "Running Automatic DVB-C scan..."
	echo "Select an option:"
	echo "1) FULL Band Scan"
	echo "2) NETWORK Scan"
	echo "3) BLIND Scan"
	echo "4) QUICK Scan"
	echo "5) Exit"

	printf "\n"
	read -r -p "Enter your choice: " choice

	case $choice in
		1)			
			printf "Scan FULL band...\n"
			curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": {"command": "Dvbc.startSearchEx", "json": ["full", "none", true, "AUTO", 0, 0, 0]}}'
			get_signal_level DVB-C
			;;	
		2)
			printf "Scan NETWORK...\n"
			curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": {"command": "Dvbc.startSearchEx", "json": ["network", "KDG", true, "AUTO", 0, 0, 0]}}'
			get_signal_level DVB-C
			;;

		3)
			printf "Scan BLIND...\n"
			curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": {"command": "Dvbc.startSearchEx", "json": ["blind", "none", true, "AUTO", 0, 0, 0]}}'
			get_signal_level DVB-C

			;;
		4)
			printf "Scan QUICK...\n"
			curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": {"command": "Dvbc.startSearchEx", "json": ["quick", "none", true, "AUTO", 0, 0, 0]}}'
			get_signal_level DVB-C
			;;
		5)
			printf "Exiting..\n"
			;;

		*)
			printf "wrong input! do nothing...\n"
			return
			;;
	esac
	sleep 1
}

function manual_dvbc_scan()
{
	# Display DVBT frequencies for each channel number
	printf "\n"
	printf "Channel Number - Frequency (MHz):\n"
	printf "2     - 50.5    3     - 57.5    4     - 64.5 \n"
	printf "5     - 177.5   6     - 184.5   7     - 191.5\n"
	printf "8     - 198.5   9     - 205.5   10    - 212.5\n"
	printf "11    - 219.5   12    - 226.5\n"

	channel_number=21
	while [ $channel_number -le 69 ]; do
		if [ $channel_number -le 61 ]; then
			frequency=$((474000000 + (channel_number - 21) * 8000000))
		else
			frequency=$((474000000 + (channel_number - 22) * 8000000))
		fi

		printf "%-5d - %-5d   " $channel_number $((frequency / 1000000))

		((channel_number++))
		if [ $((channel_number % 3)) -eq 0 ] && [ $channel_number -le 69 ]; then
			echo
		fi
	done

	printf "\n"

	read -r -p "Select a channel number (2-69): " channel_number

	if [ "$channel_number" -ge 2 ] && [ "$channel_number" -le 69 ]; then
		case $channel_number in
			2)
				frequency=50.5
				;;
			3)
				frequency=57.5
				;;
			4)
				frequency=64.5
				;;
			5)
				frequency=177.5
				;;
			6)
				frequency=184.5
				;;
			7)
				frequency=191.5
				;;
			8)
				frequency=198.5
				;;
			9)
				frequency=205.5
				;;
			10)
				frequency=212.5
				;;
			11)
				frequency=219.5
				;;
			12)
				frequency=226.5
				;;
			21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 | 32 | 33 | 34 | 35 | 36 | 37 | 38 | 39 | 40 | 41 | 42 | 43 | 44 | 45 | 46 | 47 | 48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 | 56 | 57 | 58 | 59 | 60 | 61 | 62 | 63 | 64 | 65 | 66 | 67 | 68 | 69)
				if [ $channel_number -le 61 ]; then
					frequency=$((474000000 + (channel_number - 21) * 8000000))
				else
					frequency=$((474000000 + (channel_number - 22) * 8000000))
				fi
				;;
			*)
				echo "Invalid channel number selection."
				exit 1
				;;
		esac
	else
		echo "Invalid channel number selection."
		exit 1
	fi

	# Ask for DVB-C Symbol_rate
	read -r -p "Enter Symbol Rate: 
	1) 6875 Khz
	2) 6900 Khz
	Enter the corresponding number (1-2): " dvbc_sym_rate

	case $dvbc_sym_rate in
		1)
			sym_rate="6875000"
			;;
		2)
			sym_rate="6900000"
			;;
		*)
			echo "Invalid symbol_rate. Exiting..."
			exit 1
			;;
	esac

	echo "Running Manual DVB-C scan..."
	#set -x
	curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbc.startManualSearchByFreq", "json": [true,false,'"$frequency"',"auto","'"$sym_rate"'"] }}'
	get_signal_level DVB-C
	#set +x
}

function is_dvb_playing(){

	is_playing=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Player.getStatus", "json": [] }}' | jq -r '.result | fromjson.data.state')
	is_dvb=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Player.getStatus", "json": [] }}' | jq -r '.result | fromjson.data.type')
	if [ ${is_playing} == "playing" ] || [ ${is_dvb} == "dvblive" ]
	then
		echo "true"
	else
		echo "false"
	fi
}

function enable_subtitle(){

	#TODO

	# First get list of Subtitle streams

	curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method":"DTV.1.Invoke", "params": { "command": "Player.getListOfSubtitleStreams", "json": [0] }}' 

	# If there is no Subtitle, show user an error and return to menu
	# If there is only one Subtitle, print it will be selected automatically, and then select it automatically

	curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method":"DTV.1.Invoke", "params": { "command": "Player.setSubtitleStream", "json": [0,0] }}'

	# Finally, Set the Subtitle ON
	curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method":"DTV.1.Invoke", "params": { "command": "Player.setSubtitlesOn", "json": [0, true] }}'
}

function change_audio_language(){

	case $1 in
		primary)
			lang_to_be_set="Dvb.setPrimaryAudioLangId"
			;;
		secondary)
			lang_to_be_set="Dvb.setSecondaryAudioLangId"
			;;
		*)
			printf "Error! You need to input primary or secondary as 1st arugument!\n"
			return
			;;
	esac

	

	# First check if something is being played
	if [ $(is_dvb_playing) == "true" ]
        then
                printf "playing DVB...\n"
        else
                printf "\n**********************************************\n"
                printf "!!!not playing DVB, please watch a channel first!!!\n"
                printf "*********************************************\n"
                sleep 2
                return
        fi

	# First get current country code
	country_code=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": {"command": "Dvb.getcurrentCountryInfos", "json": [] }}' | jq -r '.result | fromjson.data.country_code')

	# Second, get supported languages for this country
	printf "List of Languages for this Country:\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": {"command": "Dvb.getCountryLangs", "json": ['$country_code'] }}' | jq -r '.result | fromjson.data[]' | jq -r '"\(.lang_index)) ** \(.lang_iso639_code) ** \(.lang_ids) "' | column -t -s '**'
	
	read -r -p "Select a Primary Language (enter language number) or back to main menu (enter \"q\" ): " lang_choice
	printf "Choice is %s\n" ${lang_choice}

	if [ $lang_choice == "q" ] || [ $lang_choice == "Q" ]; then
		printf "Returning back to menu...\n"
		return
	fi


	array_of_languages=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": {"command": "Dvb.getCountryLangs", "json": [6579573] }}' | jq -r '.result | fromjson.data')

	selected_lang=$(echo "$array_of_languages" | jq -r --argjson choice "$lang_choice" '.[$choice] | .lang_index')

	printf "\nSelected Lang is $selected_lang\n"

	# Now set the selected language
	#set -x
	is_lang_set_success=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": {"command":"'$lang_to_be_set'" ,"json": ['$selected_lang'] }}' | jq -r '.result | fromjson.data')
	if [ "${is_lang_set_success}" == "true" ]
	then
		printf "Setting new audio language succesful!\n"
	else
		printf "Setting new audio language FAILED!\n"
	fi
	#set +x
}

function enable_audio_description(){

	# TODO

	# First, check if current Audio Track has Audio description or not
	# If it doesn't have, provide a list of Audio Tracks, and let user select one
	# Then check again, does this Audio track have AD or not?
	# If it doesn't have, return from this function
	# If it has, enable it as below:
	curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": {"command":"Player.setAudioDescriptionOn" ,"json": [0,true] }}' ; echo

	#And finally set AD Mix level
	curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": {"command":"Player.setADMixLevel" ,"json": [0,50] }}' ; echo

}

function set_audio_description_mix_level(){

	#TODO
	printf "nothing here...\n"
}

function set_country(){

	# TODO

	# Get list of countries first
	curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": {"command": "Dvb.getCountries", "json": [] }}'

	# Set country to new value
	curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": {"command": "Dvb.setCountry", "json": [6579573] }}'

}



function change_audio_track(){

	if [ $(is_dvb_playing) == "true" ]
	then
		printf "playing DVB...\n"
	else
		printf "\n**********************************************\n"
		printf "!!!not playing DVB, please watch a channel first!!!\n"
		printf "*********************************************\n"
		sleep 2
		return
	fi
	
	while true; do
		
		# First print audio track list
		printf "\nNo   Is_Selected Language   Codec         PID       has_AD \n"
		curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Player.getListOfAudioStreams", "json": [0] }}' | jq -r '.result | fromjson.data[] ' | jq -r '"\(.index+1)) ** \(.selected) ** \(.language) ** \(.codec) ** \(.pid) ** \(.ad) "' | column -t -s '**'

		tracks=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Player.getListOfAudioStreams", "json": [0] }}' | jq -r '.result | fromjson.data')

		printf "\nEnter q or Q to back to main menu..\n\n"
		
		read -r -p "Select an Audio Track (enter track number) or back to main menu (enter \"q\" ): " track_choice
		printf "Choice is %s\n" ${track_choice}

		if [ $track_choice == "q" ] || [ $track_choice == "Q" ]; then
			printf "Returning back to menu...\n"
			return
		fi

		# Get the URI based on the selected channel name
		selected_track=$(echo "$tracks" | jq -r --argjson choice "$track_choice" '.[$choice-1] | .index')

		printf "\nSelected Track is $selected_track\n"

		# Now set the selected track
		is_success=$(curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": {"command":"Player.setAudioStream" ,"json": [0,'$selected_track'] }}' | jq -r '.result | fromjson.data')
		if [ "${is_success}" == "true" ]
		then
			printf "Setting new audio track succesful!\n"
		else
			printf "Setting new audio track FAILED!\n"
		fi


	done


}

function dvb_menu(){

# DVB Menu

while true; do
	printf "\n"
	echo "Select an option:"
	echo "1) Watch DTV & Radio"
	echo "2) Change AUDIO Track"
	echo "3) DVB-T: Automatic Scan"
	echo "4) DVB-T: Manual Scan"
	echo "5) DVB-C: Automatic Scan"
	echo "6) DVB-C: Manual Scan"
	echo "7) DVB-S: Scan last TP"
	echo "8) DVB-S: Scan another TP / manual scan"	
	echo "9) DVB-S: EASY SCAN"
	echo "10) DVB-S: BLIND SCAN"
	echo "11) DVB-S: SAT SCAN"
	echo "12) DVB-S: NIT SCAN"
	echo "13) Stop Scanning"
	echo "14) Stop Watching"
	echo "15) Delete All satellite, all LNBs and all channels"
	echo "16) Enable Tuner Debug"
	echo "17) Disable Tuner Debug"
	echo "18) Disable Deinterlacer"
	echo "19) Change Framerate"
	echo "20) Back to Main Menu"
	echo "21) EXIT from APP"

	print_number_of_channels
	printf "\n"
	read -r -p "Enter your choice: " choice

	case $choice in
		1)
			watch_TV_RADIO all
			;;
		2) 
			change_audio_track
			;;
		3)
			stop_player
			automatic_dvbt_scan
			;;
		4)
			stop_player
			manual_dvbt_scan
			;;
		5)
			stop_player
			automatic_dvbc_scan
			;;
		6)
			stop_player
			manual_dvbc_scan
			;;
		7)
			tp_scan
			;;
		8)
			unselect_tp
			update_tp_info
			add_tp
			tp_scan
			;;
		9)
			easy_scan
			;;
		10)
			blind_scan
			;;
		11)
			satscan
			;;
		12)	
			nit_scan
			;;

		13)
			echo "Stopping Scanning..."
			stop_scan
			;;
		14)
			echo "Stopping Watching..."
			# Command to stop watching TV
			stop_player
			;;
		15)
			echo "Deleting All channels..."
			# Command to delete all channels
			stop_player
			delete_all_satellites
			delete_all_lnbs
			rm -rf /data/dtvkit.sqlite3
			rm -rf /data/persistent/dtvkit/dtvkit.sqlite3
			systemctl restart dtvkit
			printf "\nPlease wait 10 seconds to restart services...\n"
			sleep 10
			prepare_database
			;;
		16)
			printf "Enabling tuner debug...\n"
			enable_tuner_debug
			;;
		17)
			printf "Disabling tuner debug...\n"
			disable_tuner_debug
			;;
		18)
			printf "Disabling deinterlacer...\n"
			disable_deinterlacer
			;;
		19)
			printf "Modify framerate..\n"
			set_resolution_framerate
			;;		
		20)
			echo "Back to Main Menu..."
			return
			;;
		21)
			echo "Exiting..."
			exit 0
			;;

		*)
			echo "Invalid option. Please choose a valid option (1-25)."
			;;
	esac
done


}

function network_menu(){

	printf "\n NETWORK MENU...\n"

while true; do
	printf "\n"
	echo "Select an option:"
	echo "1) Play 4K Test Video"
	echo "2) Play 1080p Test Video"	
	echo "3) Back to Main Menu"
	echo "4) EXIT from APP"

	printf "\n"
	read -r -p "Enter your choice: " choice

	case $choice in
		1)
			printf "Play 4K Test Video..."
			printf "Press Q on your keyboard to stop playback !!!\n"
			sleep 1
			stop_player
			prepare_display
			gst-play-1.0 https://dash.akamaized.net/akamai/4k/Kluge.mpd
			;;
		2)
			echo "Play 1080p Test Video"
			printf "Press Q on your keyboard to stop playback !!!\n"
			stop_player
			prepare_display
			gst-play-1.0 http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4
			;;
		3)
			echo "Back to Main Menu..."
			return
			;;
		4)
			echo "Exiting..."
			exit 0
			;;

		*)
			echo "Invalid option. Please choose a valid option (1-9)."
			;;
	esac
done
}

function launch_hbbtv_atmos(){

        printf "\nCreating Display\n"
        curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.createDisplay", "params": { "client": "test-0", "displayName": "test-0" }}'
        printf "\nMoving Display to Front\n"
        curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.moveToFront", "params": { "client": "test-0" }}'
        printf "\nSetting Focus to Display\n"
        curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.setFocus", "params": { "client": "test-0" }}'

	export XDG_RUNTIME_DIR=/run
	export WESTEROS_GL_GRAPHICS_MAX_SIZE=1920x1080
	export WESTEROS_SINK_AMLOGIC_USE_DMABUF=1
	export WESTEROS_GL_USE_REFRESH_LOCK=1
	export WESTEROS_SINK_USE_FREERUN=1
	export WESTEROS_GL_USE_AMLOGIC_AVSYNC=1
	export LD_PRELOAD=/usr/lib/libwesteros_gl.so.0.0.0
	export WAYLAND_DISPLAY=test-0

	printf "\n"
	printf "Activate Controller\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "Controller.1.activate", "params": { "callsign": "WebKitBrowser" }}'
	printf "Enable Local Storage\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "WebKitBrowser.1.localstorageenabled", "params": true}'
	printf "Set browser state to Resumed\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "WebKitBrowser.1.state", "params": "resumed"}'
	printf "Set focus to WebKitBrowser\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.setFocus", "params": { "client": "WebKitBrowser" }}'
	printf "Set URL to open in browser\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc":"2.0","id":3, "method":"WebKitBrowser.1.url", "params": "http://ms12.streaming.dolby.com/v26/app/index_261.html"}'
	printf "Set visibility of residentapp / launcher to false..\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc":"2.0","id":3, "method":"org.rdk.RDKShell.1.setVisibility","params":{"client":"residentapp","visible": false}}'


}


function dolby_menu(){
	
	printf "\n DOLBY MENU...\n"

while true; do
	printf "\n"
	echo "Select an option:"
	echo "1) Dolby Vision Test"
	echo "2) Dolby Audio Test"
	echo "3) Dolby ATMOS Test"
	echo "4) Dolby cert HBBTV Test"
	echo "5) Back to Main Menu"
	echo "6) EXIT from APP"


	printf "\n"
	read -r -p "Enter your choice: " choice

	case $choice in
		1)
			echo "Dolby Vision test"
			printf "Press Q on your keyboard to stop playback !!!\n"
			stop_player
			prepare_display
			gst-play-1.0 http://kernelmax.com:9000/dva.mp4
			;;
		2)
			echo "Dolby Audio test"
			printf "Press Q on your keyboard to stop playback !!!\n"
			stop_player
			prepare_display
			gst-play-1.0 http://kernelmax.com:9000/dolby-audio.mp4
			;;
		3)
			echo "Dolby ATMOS test"
			printf "Press Q on your keyboard to stop playback !!!\n"
			stop_player
			prepare_display
			gst-play-1.0 http://kernelmax.com:9000/Universe_Fury2.mp4
			;;
		4)
			echo "Dolby cert HBBTV test"
			launch_hbbtv_atmos
			;;
		5)
			echo "Back to Main Menu..."
			return
			;;
		6)
			echo "Exiting..."
			exit 0
			;;
		*)
			echo "Invalid option. Please choose a valid option (1-9)."
			;;
	esac
done

}


#First check if DTVkit system service is ready or not
is_dtvkit_ready

prepare_database


if [ $# -eq 4 ]; then
	FREQ=$1
	POL=$2
	SYMRATE=$3
	DVBTYPE=$4

	add_tp
	tp_scan
fi


# Main Menu
while true; do
	printf "\n"
	echo "Select an option:"
	echo "1) DVB Scan & Play menu"
	echo "2) Network Video Play menu"
	echo "3) DOLBY Test menu"
	echo "4) Back to RDK Launcher"
	echo "5) EXIT"

	print_number_of_channels
	printf "\n"
	read -r -p "Enter your choice: " choice

	case $choice in
		1)	
			dvb_menu
			;;
		2)
			network_menu
			;;
		3)
			dolby_menu
			;;
		4)
			echo "Back to RDK Launcher"
			stop_player
			back_to_launcher
			;;
		5)
			echo "Exiting..."
			exit 0
			;;
		*)
			echo "Invalid option. Please choose a valid option (1-9)."
			;;
	esac
done

# old SINGLE PAGE Menu , will not be used anymore

exit 1

while true; do
	printf "\n"
	echo "Select an option:"
	echo "1) DVB-T: Automatic Scan"
	echo "2) DVB-T: Manual Scan"
	echo "3) DVB-C: Automatic Scan"
	echo "4) DVB-C: Manual Scan"
	echo "5) DVB-S: Scan last TP"
	echo "6) DVB-S: Scan another TP / manual scan"
	echo "7) Stop Scanning"
	echo "8) Watch TV"
	echo "9) Stop Watching"
	echo "10) Delete All satellite, all LNBs and all channels"
	echo "11) Back to RDK Launcher"
	echo "12) Play 4K Test Video"
	echo "13) Play 1080p Test Video"
	echo "14) EXIT"
	echo "15) Enable Tuner Debug"
	echo "16) Disable Tuner Debug"
	echo "17) Disable Deinterlacer"
	echo "18) Change Framerate"
	echo "19) DVB-S: EASY SCAN"
	echo "20) DVB-S: BLIND SCAN"
	echo "21) DVB-S: SAT SCAN"
	echo "22) DVB-S: NIT SCAN"
	echo "23) Listen Radio"
	echo "24) Dolby Vision Test"
	echo "25) Dolby Audio Test"
	echo "26) Dolby ATMOS Test"

	print_number_of_channels
	printf "\n"
	read -r -p "Enter your choice: " choice

	case $choice in
		1)
			stop_player
			automatic_dvbt_scan
			;;
		2)
			stop_player
			manual_dvbt_scan
			;;
		3)
			stop_player
			automatic_dvbc_scan
			;;
		4)
			stop_player
			manual_dvbc_scan
			;;
		5)
			tp_scan
			;;
		6)
			unselect_tp
			update_tp_info
			add_tp
			tp_scan
			;;
		7)
			echo "Stopping Scanning..."
			stop_scan
			;;
		8)
			watch_TV_RADIO all
			;;

		9)
			echo "Stopping Watching..."
			# Command to stop watching TV
			stop_player
			;;
		10)
			echo "Deleting All channels..."
			# Command to delete all channels
			stop_player
			delete_all_satellites
			delete_all_lnbs
			rm -rf /data/dtvkit.sqlite3
			rm -rf /data/persistent/dtvkit/dtvkit.sqlite3
			systemctl restart dtvkit
			printf "\nPlease wait 10 seconds to restart services...\n"
			sleep 10
			prepare_database
			;;
		11)
			echo "Back to RDK Launcher"
			stop_player
			back_to_launcher
			;;
		12)
			printf "Play 4K Test Video..."
			printf "Press Q on your keyboard to stop playback !!!\n"
			sleep 1
			stop_player
			prepare_display
			gst-play-1.0 https://dash.akamaized.net/akamai/4k/Kluge.mpd
			;;
		13)
			echo "Play 1080p Test Video"
			printf "Press Q on your keyboard to stop playback !!!\n"
			stop_player
			prepare_display
			gst-play-1.0 http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4
			;;
		14)
			echo "Exiting..."
			exit 0
			;;
		15)
			printf "Enabling tuner debug...\n"
			enable_tuner_debug
			;;
		16)
			printf "Disabling tuner debug...\n"
			disable_tuner_debug
			;;
		17)
			printf "Disabling deinterlacer...\n"
			disable_deinterlacer
			;;
		18)
			printf "Modify framerate..\n"
			set_resolution_framerate
			;;
		19)
			easy_scan
			;;
		20)
			blind_scan
			;;
		21)
			satscan
			;;
		22)	
			nit_scan
			;;
		23)
			watch_TV_RADIO all
			;;
		24)
			echo "Dolby Vision test"
			printf "Press Q on your keyboard to stop playback !!!\n"
			stop_player
			prepare_display
			gst-play-1.0 http://kernelmax.com:9000/dva.mp4
			;;
		25)
			echo "Dolby Audio test"
			printf "Press Q on your keyboard to stop playback !!!\n"
			stop_player
			prepare_display
			gst-play-1.0 http://kernelmax.com:9000/dolby-audio.mp4
			;;
		26)
			echo "Dolby ATMOS test"
			printf "Press Q on your keyboard to stop playback !!!\n"
			stop_player
			prepare_display
			gst-play-1.0 http://kernelmax.com:9000/Universe_Fury2.mp4
			;;

		*)
			echo "Invalid option. Please choose a valid option (1-9)."
			;;
	esac
done
