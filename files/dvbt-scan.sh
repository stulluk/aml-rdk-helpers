#!/bin/bash

#set -x

DEV_IP_PORT="127.0.0.1:9998"

function is_dtvkit_ready(){

	if systemctl is-active --quiet  dtvkit
	then
		printf "DTVkit service is ready, starting..\n"
	else
		printf "DTVkit service is NOT ready, please wait a while and re-run this script..\n"
		exit 1
	fi


}

function prepare_display()
{
	#Prepare environment
	echo 1 > /sys/kernel/debug/dri/0/vpu/blank

	printf "\nCreating Display\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.createDisplay", "params": { "client": "test-0", "displayName": "test-0" }}'
	printf "\nMoving Display to Front\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.moveToFront", "params": { "client": "test-0" }}'
	printf "\nSetting Focus to Display\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.setFocus", "params": { "client": "test-0" }}'

	export XDG_RUNTIME_DIR=/run
	export WESTEROS_GL_GRAPHICS_MAX_SIZE=3840x2160
	# # USE_DMABUF is needed
	export WESTEROS_SINK_AMLOGIC_USE_DMABUF=1
	export PLAYREADY_OP_DIGITAL_LEVEL=200
	export WESTEROS_GL_USE_REFRESH_LOCK=1
	export WESTEROS_SINK_USE_FREERUN=1
	export WESTEROS_GL_USE_AMLOGIC_AVSYNC=1
	export WESTEROS_SINK_USE_ESSRMGR=1
	export LD_PRELOAD=libwesteros_gl.so.0.0.0
	export WAYLAND_DISPLAY=test-0
}

function back_to_launcher()
{
	# After test, recover OSD
	echo 0 > /sys/kernel/debug/dri/0/vpu/blank
	# recover OSD focus
	curl 'http://${DEV_IP_PORT}/jsonrpc' -d '{"jsonrpc": "2.0","id": 4,"method": "org.rdk.RDKShell.1.setFocus", "params": { "client": "residentapp" }}'
	echo
}

function stop_player()
{

	#First stop scanning
	printf "\nFinishing Search\n"
	curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbt.finishSearch", "json": [true] }}'

	#Now stop player
	printf "\nStop Playback\n"
	curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Player.stop", "json": [0] }}'
	printf "\n"
}

function get_frequency()
{
	channel_number=$1
	case $channel_number in
	2)
		echo "50.5"
		;;
	3)
		echo "57.5"
		;;
	4)
		echo "64.5"
		;;
	5)
		echo "177.5"
		;;
	6)
		echo "184.5"
		;;
	7)
		echo "191.5"
		;;
	8)
		echo "198.5"
		;;
	9)
		echo "205.5"
		;;
	10)
		echo "212.5"
		;;
	11)
		echo "219.5"
		;;
	12)
		echo "226.5"
		;;
	21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 | 32 | 33 | 34 | 35 | 36 | 37 | 38 | 39 | 40 | 41 | 42 | 43 | 44 | 45 | 46 | 47 | 48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 | 56 | 57 | 58 | 59 | 60 | 61 | 62 | 63 | 64 | 65 | 66 | 67 | 68 | 69)
		echo $((474 + ($channel_number - 21) * 8))
		;;
	*)
		echo "Invalid channel number"
		;;
	esac
}

function manual_dvbt_scan()
{
	# Display DVBT frequencies for each channel number
	printf "\n"
	printf "Channel Number - Frequency (MHz):\n"
	printf "2     - 50.5    3     - 57.5    4     - 64.5 \n"
	printf "5     - 177.5   6     - 184.5   7     - 191.5\n"
	printf "8     - 198.5   9     - 205.5   10    - 212.5\n"
	printf "11    - 219.5   12    - 226.5\n"

	channel_number=21
	while [ $channel_number -le 69 ]; do
		if [ $channel_number -le 61 ]; then
			frequency=$((474000000 + (channel_number - 21) * 8000000))
		else
			frequency=$((474000000 + (channel_number - 22) * 8000000))
		fi

		printf "%-5d - %-5d   " $channel_number $((frequency / 1000000))

		((channel_number++))
		if [ $((channel_number % 3)) -eq 0 ] && [ $channel_number -le 69 ]; then
			echo
		fi
	done

	printf "\n"

	read -p "Select a channel number (2-69): " channel_number

	if [ "$channel_number" -ge 2 ] && [ "$channel_number" -le 69 ]; then
		case $channel_number in
		2)
			frequency=50.5
			;;
		3)
			frequency=57.5
			;;
		4)
			frequency=64.5
			;;
		5)
			frequency=177.5
			;;
		6)
			frequency=184.5
			;;
		7)
			frequency=191.5
			;;
		8)
			frequency=198.5
			;;
		9)
			frequency=205.5
			;;
		10)
			frequency=212.5
			;;
		11)
			frequency=219.5
			;;
		12)
			frequency=226.5
			;;
		21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 | 32 | 33 | 34 | 35 | 36 | 37 | 38 | 39 | 40 | 41 | 42 | 43 | 44 | 45 | 46 | 47 | 48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 | 56 | 57 | 58 | 59 | 60 | 61 | 62 | 63 | 64 | 65 | 66 | 67 | 68 | 69)
			if [ $channel_number -le 61 ]; then
				frequency=$((474000000 + (channel_number - 21) * 8000000))
			else
				frequency=$((474000000 + (channel_number - 22) * 8000000))
			fi
			;;
		*)
			echo "Invalid channel number selection."
			exit 1
			;;
		esac
	else
		echo "Invalid channel number selection."
		exit 1
	fi

	# Ask for DVB-T type
	read -p "Select DVB-T Type: 
    1) DVB-T 
    2) DVB-T2
    Enter the corresponding number (1-2): " dvbt_type

	case $dvbt_type in
	1)
		dvbt_type="DVB-T"
		# Bandwidth can be one of 5Mhz, 6Mhz, 7Mhz, and 8Mhz
		read -p "Select Bandwidth:
		    1) 5Mhz
		    2) 6Mhz
		    3) 7Mhz
		    4) 8Mhz
		    Enter the corresponding number (1-4): " bandwidth_option

		case $bandwidth_option in
		1)
			bandwidth="5Mhz"
			;;
		2)
			bandwidth="6Mhz"
			;;
		3)
			bandwidth="7Mhz"
			;;
		4)
			bandwidth="8Mhz"
			;;
		*)
			echo "Invalid bandwidth option. Exiting..."
			exit 1
			;;
		esac

		# FFT mode can be 2K, 4K, and 8K
		read -p "Select FFT Mode: 
	    1) 2K 
	    2) 4K 
	    3) 8K
	    Enter the corresponding number (1-3): " fft_option
		case $fft_option in
		1)
			fft_mode="2K"
			;;
		2)
			fft_mode="4K"
			;;
		3)
			fft_mode="8K"
			;;
		*)
			echo "Invalid FFT mode input. Exiting..."
			exit 1
			;;
		esac
		;;
	2)
		dvbt_type="DVB-T2"
		# Bandwidth can be one of 1.7Mhz, 5Mhz, 6Mhz, 7Mhz, 8Mhz, and 10Mhz
		read -p "Select Bandwidth:
	    1) 1.7Mhz
	    2) 5Mhz
	    3) 6Mhz
	    4) 7Mhz
	    5) 8Mhz
	    6) 10Mhz
	    Enter the corresponding number (1-6): " bandwidth_option

		case $bandwidth_option in
		1)
			bandwidth="1.7Mhz"
			;;
		2)
			bandwidth="5Mhz"
			;;
		3)
			bandwidth="6Mhz"
			;;
		4)
			bandwidth="7Mhz"
			;;
		5)
			bandwidth="8Mhz"
			;;
		6)
			bandwidth="10Mhz"
			;;
		*)
			echo "Invalid bandwidth option. Exiting..."
			exit 1
			;;
		esac

		# FFT mode can be 1K, 2K, 4K, 8K, 16K, and 32K
		read -p "Select FFT Mode: 
	    1) 1K
	    2) 2K 
	    3) 4K 
	    4) 8K
	    5) 16K
	    6) 32K
	    Enter the corresponding number (1-6): " fft_option
		case $fft_option in
		1)
			fft_mode="1K"
			;;
		2)
			fft_mode="2K"
			;;
		3)
			fft_mode="4K"
			;;
		4)
			fft_mode="8K"
			;;
		5)
			fft_mode="16K"
			;;
		6)
			fft_mode="32K"
			;;
		*)
			echo "Invalid FFT mode input. Exiting..."
			exit 1
			;;
		esac
		;;
	*)
		echo "Invalid DVB-T type input. Exiting..."
		exit 1
		;;
	esac

	echo "Running Manual DVBT scan..."
	# Command for Manual DVBT scan
	curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbt.startManualSearchByFreq", "json": [true,false,'"$frequency"',"'"$bandwidth"'","'"$fft_mode"'","'"$dvbt_type"'"] }}'
}

function enable_debug(){

	#check board first
	if [ -e /sys/module/si2151_fe_64/parameters/si2151_debug ]
	then
		echo "1" > /sys/module/si2151_fe_64/parameters/si2151_debug
	fi

	if [ -e /sys/module/r842_fe_64/parameters/r842_debug ]
	then
		echo "1" > /sys/module/r842_fe_64/parameters/r842_debug
	fi

	# Enable DEMOD debug
	echo 0xfff > /sys/module/aml_media/parameters/aml_demod_debug

}

function disable_debug(){

	#check board first
        if [ -e /sys/module/si2151_fe_64/parameters/si2151_debug ]
        then
		echo "0" > /sys/module/si2151_fe_64/parameters/si2151_debug
        fi

        if [ -e /sys/module/r842_fe_64/parameters/r842_debug ]
        then
                echo "0" > /sys/module/r842_fe_64/parameters/r842_debug
        fi

        # Enable DEMOD debug
        echo 0x0 > /sys/module/aml_media/parameters/aml_demod_debug

}

function list_channels() {

	numofch=$(get_number_of_channels)
	if [ "${numofch}" -lt 1 ]
	then
		printf "No channels in DB!!!\n"
		exit 1
	fi

	printf "Number of Channel in DB is %d\n" "${numofch}"

	printf "ChNO - CH Name -   Frequency - DVB Type\n"

	#Lets read whole CHLIST chunk-by-chunk, 50 channels at a time
        offset=0
        increment=25

	while [[ $offset -lt $numofch ]]
        do
		curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getListOfServicesByIndex", "json": ["cur","all",'$offset','$increment'] }}' | jq -c '.result | fromjson.data[]' | jq -r '"\(input_line_number+'$offset')) \(.name) ** \(.transponder) \(.freq/1000000) ** \(.sig_name)"' | column -t -s '**'
		offset=$((offset+increment))
        done
}

function watch_TV_RADIO() {
	
	tvradio=$1

	# First check if we have any channel in the list

	numofch=$(get_number_of_channels)
        if [ "${numofch}" -lt 1 ]
        then
                printf "No channels in DB!!!\n"
                exit 1
        fi

        printf "Preparing Channel List..."
        # Define the base JSON request
	#set -x
        base_request='{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getListOfServicesByIndex", "json": ["cur",'\"${tvradio}\"','
	#set +x

        offset=0
        increment=25

        services=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d "$base_request$offset,$increment]}}" | jq '.result | fromjson.data')
        offset=$((offset+increment))

        while [[ $offset -lt $numofch ]]
        do
                printf "."
                # Make the curl request and append the result to the services variable
                curservices=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d "$base_request$offset,$increment]}}" | jq '.result | fromjson.data')
                services=$(echo $services $curservices | jq -s add)
                offset=$((offset+increment))
        done

	printf "\nChannel list is created...\n"
	#services=$(construct_services_array) #Previously I was using this function but I gave up because below 20 lines can display a small progress when constrocting more than 500channels..

	# Check if the result field is an empty string or contains an error message
	is_valid_json=$(echo "$services" | jq -e . >/dev/null 2>&1 ; echo ${PIPESTATUS[1]})
	if [ "${is_valid_json}" == "1" ] 
	then
		printf "\n\n************\nNo channels, please SCAN first.\n*************\n\n\n"
		printf "\n\n************\nNo VALID JSON, system ERROR\n*************\n\n\n"
	elif [ "${is_valid_json}" == "4" ]
	then
		printf "\n\n************\nNo VALID JSON, system ERROR\n*************\n\n\n"
		printf "\n\n************\nNo channels, please SCAN first.\n*************\n\n\n"
	else
		while [ 1 ]; do

			printf "\nCHANNEL LIST\n"
			# Parse the JSON and print the names and URIs
			list_channels
			#curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getListOfServicesByIndex", "json": ["cur","all",0,50] }}' | jq -c '.result | fromjson.data[]' | jq -r '"\(input_line_number)) \(.name) ** \(.transponder) ** \(.sig_name)"' | column -t -s '**'
			printf "\nEnter q or Q to back to main menu..\n\n"
			read -p "Select a channel (enter ch number) or back to main menu (enter \"q\" ): " channel_choice
			printf "Choice is %s\n" ${channel_choice}

			if [ $channel_choice == "q" ] || [ $channel_choice == "Q" ]; then
				printf "Returning back to main menu...\n"
				return
			fi

			# Get the URI based on the selected channel name
			selected_uri=$(echo "$services" | jq -r --argjson choice "$channel_choice" '.[$choice-1] | .uri')

			printf "\nSelected URI is $selected_uri\n"

			stop_player
			prepare_display

			# Command to start watching the selected channel
			printf "\nStart playing ${selected_uri}...\n"
			curl -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Player.play", "json": [0,"'"$selected_uri"'",false,false,0,"",""] }}'
			printf "\n\n"
		done
	fi

}

function get_number_of_channels(){

	numofch=$(curl -s -X POST http://$DEV_IP_PORT/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvb.getNumberOfServices", "json": [] }}' | jq -r '.result | fromjson.data')
	echo $numofch
}

#First check if DTVkit is ready or not
is_dtvkit_ready

#by default, enable debug
enable_debug

while true; do
	printf "\n"
	echo "Select an option:"
	echo "1) Automatic DVBT scan"
	echo "2) Manual DVBT Scan"
	echo "3) Stop Scanning"
	echo "4) Watch TV"
	echo "5) Stop Watching"
	echo "6) Delete All channels"
	echo "7) Back to RDK Launcher"
	echo "8) Play 4K Test Video"
	echo "9) Play 1080p Test Video"
	echo "10) EXIT"
	echo "11) enable_debug"
	echo "12) disable_debug"

	printf "\n"
	read -p "Enter your choice: " choice

	case $choice in
	1)
		stop_player
		echo "Running Automatic DVBT scan..."
		sleep 1
		# Command for Automatic DVBT scan
		curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbt.startSearch", "json": [true,false] }}'
		sleep 1
		;;
	2)
		stop_player
		manual_dvbt_scan
		;;
	3)
		echo "Stopping Scanning..."
		# Command to stop scanning
		curl -s -X POST http://${DEV_IP_PORT}/jsonrpc -d '{"jsonrpc": "2.0","id": 1,"method": "DTV.1.Invoke", "params": { "command": "Dvbt.finishSearch", "json": [true] }}'
		sleep 1
		;;
	4)
		printf "\nWatch TV / RADIO \n..."
		watch_TV_RADIO tv
		;;
	5)
		echo "Stopping Watching..."
		# Command to stop watching TV
		stop_player
		;;
	6)
		echo "Deleting All channels..."
		# Command to delete all channels
		stop_player
		rm -rf /data/dtvkit.sqlite3
		rm -rf /data/persistent/dtvkit/dtvkit.sqlite3
		systemctl restart dtvkit
		printf "\nPlease wait 10 seconds to restart services...\n"
		sleep 10
		;;
	7)
		echo "Back to RDK Launcher"
		stop_player
		back_to_launcher
		;;
	8)
		printf "Play 4K Test Video..."
		printf "Press Q on your keyboard to stop playback !!!\n"
		sleep 1
		stop_player
		prepare_display
		gst-play-1.0 https://dash.akamaized.net/akamai/4k/Kluge.mpd
		;;
	9)
		echo "Play 1080p Test Video"
		printf "Press Q on your keyboard to stop playback !!!\n"
		stop_player
		prepare_display
		gst-play-1.0 http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4
		;;
	10)
		echo "Exiting..."
		exit 0
		;;	
	11)
		echo "Enabling debug..."
		enable_debug
		;;
	12)
		echo "Disabling debug..."
		disable_debug
		;;


	*)
		echo "Invalid option. Please choose a valid option (1-9)."
		;;
	esac
done
